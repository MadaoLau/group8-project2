import { Request, Response, NextFunction } from "express"
import "./session"

export function adminOnly(req: Request, res: Response, next: NextFunction) {
  if (req.session.user?.is_admin) {
    next()
  } else {
    res.status(401).json({ message: "Only accessible by admin",
    error: "Only accessible by admin", })
  }
}

export function memberOnly(req: Request, res: Response, next: NextFunction) {
  if (req.session.user) {
    next()
  } else {
    console.log("401:", req.method, req.url)
    res
      .status(401)
      .json({
        message: "Only accessible by user",
        error: "Only accessible by user",
      })
  }
}

export function visitor(req: Request, res: Response, next: NextFunction) {
  if (!req.session.user) {
    next()
  } else {
    res.status(401).json({ message: "Only accessible by visitor" })
  }
}
