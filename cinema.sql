create table "user"(
    id serial primary key
    , user_create_date TIMESTAMP
    , user_name varchar(255) not null
    , user_password varchar(255) not null
    , email varchar(255)
    , user_icon varchar(255)
);

create table "arts"(
    id serial primary key,
    title varchar(255) not null,
    upfilename varchar(255) null,
    intro text not null
);

alter table arts add column artist varchar(255);
alter table arts add column member_id integer, add foreign key(member_id) references member(id);

create table arts_history (
	id serial primary key
	, arts_id integer not null references music(id)
    , arts_name varchar(255)
	, arts_content text
    , created_at timestamp default current_timestamp
);

create table comments (
    id serial primary key,
    content text,
    member_id references user(id)
)