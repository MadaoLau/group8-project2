import express from "express"
import path from "path"
import { print } from "listening-on"
import { adminOnly } from "./guard"
import { attachUserRoutes } from "./user"
// import { client } from "./db"
import { musicRouter } from "./music"
import { attachSession } from "./session"
import { vTuberRouter } from "./server/vtuber"
// import multer from "multer"
import { videoRouter } from "./server/video"
import {artsRouter} from "./cinema"

let app = express()

attachSession(app)

app.use(express.urlencoded({ extended: true }))
app.use(express.json())

app.use(express.static("public"))
app.use(express.static("uploads"))

app.use((req, res, next) => {
  console.log(req.method, req.url)
  next()
})

attachUserRoutes(app)
app.use(musicRouter)
app.use(vTuberRouter)
app.use(videoRouter)
app.use(artsRouter)

// app.get('/vtubers', (req, res) => {
// 	res.sendFile(path.resolve(path.join('public', 'vtuber.html')))
// })
// app.get('/video', (req, res) => {
// 	res.sendFile(path.resolve(path.join('public', 'video.html')))
// })

app.use("/admin", adminOnly, express.static("admin"))

app.use((req, res) => {
  console.log("404:", req.method, req.url)
  res.sendFile(path.resolve(path.join("public", "404.html")))
})

/////////////Register route Simon////////////////////////////////////////
// app.post('/register', async (req, res) => {
//     let username = req.body.username
//     let password = req.body.password1
//     let hp = await hashPassword(password)

//     let result = (await client.query('SELECT * FROM members WHERE username = $1', [username]))
//     if (result.rowCount > 0) {
//       return res.status(401).end()
//     }
//     await client.query('INSERT INTO members (username,password) VALUES ($1,$2)', [username, hp])
//     return res.end()

//   })
////////////////////Register Route Simon/////////////////////////////////////////////////////





////////////////////////////Submit cinema form//////////////////////////////////////////////////////////
// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, path.resolve(__dirname, "uploads"))
//   },
//   filename: function (req, file, cb) {
//     cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`)
//   },
// })

// const upload = multer({ storage })

// type cinemaForm = {
//   name: string
//   photo?: string
//   content: string
// }

// app.post("/cinemaSubmit", upload.single("image"), async (req, res) => {
//   //console.log(req.body)
//   const imageFileName = req.file?.filename
//   // @ts-ignore
//   const submitForm: cinemaForm = {
//     name: req.body.name,
//     content: req.body.content,
//     photo: imageFileName,
//   }
//   const cinemaSubmitted = await client.query(
//     `insert into arts(title, upfilename, content) values (${submitForm.name},${submitForm.photo},${submitForm.content})`,
//   )
//   console.log(cinemaSubmitted)
//   res.json({ message: "Info uploaded" })
// })
////////////////////////////Submit cinema form//////////////////////////////////////////////////////////
// app.get ('/logout', async function logout(req,res) {
// 	if (req.session['user']){
// 		delete req.session['user'];
// 	}
// 	// res.redirect('/');
// 	console.log("logged out.")
// 	return res.end()
// })

let port = 8100

app.listen(port, () => {
  print(port)
})
