import express from "express"
// import { client } from "./db"
// import { upload } from './upload'
import { client } from "./db"
import multer from "multer"
import { memberOnly } from "./guard"

export let musicRouter = express.Router()

let fileCounter = 0 //防止同時間上傳撞名
const storage = multer.diskStorage({
  destination: "./uploads",
  filename: function (req, file, cb) {
    fileCounter++
    let ext: string = file.mimetype.split("/").pop()!
    ext = ext.split("-").pop()!
    ext = ext.split(";")[0]
    //console.log({ file, ext });
    if (file.fieldname == "music_photo") {
      switch (ext) {
        //TODO: add all mimetype of photo
        case "jpeg":
        case "png":
        case "gif":
          cb(null, `${file.fieldname}-${Date.now()}-${fileCounter}.${ext}`)
          break
        default:
          req.res?.status(400).end("Invalid image format")
          cb(new Error("invalid image format"), "")
          break
      }
    } else if (file.fieldname == "music_file") {
      switch (ext) {
        //TODO: add all mimetype of audio
        case "mpeg":
        case "mp3":
          cb(null, `${file.fieldname}-${Date.now()}-${fileCounter}.${ext}`)
          break
        default:
          req.res?.status(400).end("Invalid music format")
          cb(new Error("invalid music format"), "")
          break
      }
    } else {
      req.res?.status(400).end("Invalid field")
      cb(new Error("invalid field"), "")
    }
  },
})
let uploadMusic = multer({ storage })

export type Music = {
  id: number
  music_name: string
  music_photo: string | null
  music_content: string
  music_file: string | null
  member_id: number
  music_type: string
  singer_id: number | null
}

export type Post = {
  id: number
  user_id: number
  content: string
  reply_id: number | null
  music_id: number | null
  singer_id: number | null
}

export type InsertRow<Row> = Omit<Row, "id">
// export type MusicHistory = {
//   id: number
//   music_id: number
//   music_name: string
//   music_content: string
// }

// export type MusicListItem = Music & {
//   history: string[]
// }

async function loadCreateSingerList() {
  let page = 1
  let itemPerPage = 4
  itemPerPage *= 100

  let limit = itemPerPage
  let offset = (page - 1) * itemPerPage
  let results = await client.query(
    /* sql */ `
    select singer.*, 
    (select count(*) from user_log where user_log.singer_id = singer.id) as singerviews,
    music.music_name, music.music_file
    from singer
    left join music on music.singer_id = singer.id 
    limit $1
    offset $2
        `,
    [limit, offset],
  )

  console.log(results.rows)
  let createSingerList = results.rows.map((singers) => {
    return {
      ...singers,
    }
  })
  console.log(createSingerList)
  return createSingerList
}

async function loadSingerList() {
  let page = 1
  let itemPerPage = 4
  itemPerPage *= 100

  let limit = itemPerPage
  let offset = (page - 1) * itemPerPage
  let result = await client.query(
    /* sql */ `
    select singer.*, 
    (select count(*) from user_log where user_log.singer_id = singer.id) as singerviews
    from singer
    order by singerviews desc  
    limit $1
    offset $2
        `,
    [limit, offset],
  )
  let singerList = result.rows.map((singer) => {
    return {
      ...singer,
    }
  })
  return singerList
}

async function loadMusicList(): Promise<Music[]> {
  let limit = 100
  let offset = 0
  let result = await client.query(
    /* sql */ `
    select music.*, 
    (select count(*) from user_log where user_log.music_id = music.id) as views,
    member.username, member.icon, singer.singer_name
    from music
    left join member on member.id = music.member_id
    left join singer on singer.id = music.singer_id
    order by views desc    
    limit $1
    offset $2
        `,
    [limit, offset],
  )
  let dbMusicList: Music[] = result.rows
  // let musicUserSinger
  // musicUserSinger

  let musicList: Music[] = dbMusicList.map((music) => {
    return {
      ...music,
    }
  })
  return musicList
}

/////////////////////////////////////////////////////////////////////////
// result = await client.query(
//   /* sql */ `select music_id, music_name, music_content from music_history where id in(
//           select id from music limit $1 offset $2
//       )`,
//   [limit, offset],
// )
// let musicHistoryList: Pick<
//   MusicHistory,
//   "music_id" | "music_name" | "music_content"
// >[] = result.rows
// musicList.forEach((history) => {
//   let music = musicList.find((music) => music.id)
//   if (music) {
//     music.history.push(history.music_content)
//   }
// })
// musicList.push(postUser)
////////////////////////////////////////////////////////////////////////////////

async function loadPostList() {
  let page = 1
  let itemPerPage = 4
  itemPerPage *= 100

  let limit = itemPerPage
  let offset = (page - 1) * itemPerPage
  let result = await client.query(
    /* sql */ `
        select music_post.*, member.username, member.icon
        from music_post
        left join member on member.id = music_post.user_id
        order by updated_at desc
        limit $1
        offset $2
        `,
    [limit, offset],
  )
  let dbPostList = result.rows
  let postList = dbPostList.map((music_post) => {
    return {
      ...music_post,
    }
  })
  return postList
}

async function loadSingerPostList() {
  let page = 1
  let itemPerPage = 4
  itemPerPage *= 100

  let limit = itemPerPage
  let offset = (page - 1) * itemPerPage
  let result = await client.query(
    /* sql */ `
        select singer_post.*, member.username, member.icon
        from singer_post
        left join member on member.id = singer_post.user_id
        order by updated_at desc
        limit $1
        offset $2
        `,
    [limit, offset],
  )
  let dbPostList = result.rows
  let singerPostList = dbPostList.map((singer_post) => {
    return {
      ...singer_post,
    }
  })
  return singerPostList
}

musicRouter.get("/music/singer/create", async (req, res) => {
  try {
    let createSingerList = await loadCreateSingerList()
    // console.log(createSingerList)
    res.json({ createSingerList })
  } catch (error) {
    console.error("Failed to load create singer list:", error)
    res.status(500).json({ message: "GET: Database Error" })
  }
})

musicRouter.get("/music-singer", async (req, res) => {
  try {
    let singerList = await loadSingerList()
    res.json({ singerList })
  } catch (error) {
    console.error("Failed to load singer list:", error)
    res.status(500).json({ message: "GET: Database Error" })
  }
})

musicRouter.get("/music", async (req, res) => {
  try {
    let musicList = await loadMusicList()
    res.json({ musicList })
  } catch (error) {
    console.error("Failed to load music list:", error)
    res.status(500).json({ message: "GET: Database Error" })
  }
})

musicRouter.get("/music/:id/share", async (req, res) => {
  try {
    let postList = await loadPostList()
    res.json({ postList })
  } catch (error) {
    console.error("Failed to load comment list:", error)
    res.status(500).json({ message: "GET: Database Error" })
  }
})

musicRouter.get("/singer/:id/comment", async (req, res) => {
  try {
    let singerPostList = await loadSingerPostList()
    res.json({ singerPostList })
  } catch (error) {
    console.error("Failed to load comment list:", error)
    res.status(500).json({ message: "GET: Database Error" })
  }
})

musicRouter.post(
  "/music",
  memberOnly,
  uploadMusic.fields([
    { name: "music_photo", maxCount: 1 },
    { name: "music_file", maxCount: 1 },
  ]),
  async (req, res) => {
    if (!req.body.music_type) {
      res.status(400).json({ message: "Missing music type" })
      return
    }

    if (!req.body.music_name || typeof req.body.music_name != "string") {
      res.status(400).json({ message: "Missing music name" })
      return
    }
    let singerResult
    let singerId
    let member = req.session.user!

    let photo_filename
    let music_filename
    if (req.files) {
      if (Array.isArray(req.files)) {
        for (let file of req.files) {
          if (file.fieldname == "music_photo") {
            photo_filename = file.filename
          } else if (file.fieldname == "music_file") {
            music_filename = file.filename
          }
        }
      } else {
        photo_filename = req.files.music_photo?.[0].filename
        music_filename = req.files.music_file?.[0].filename
      }
    }

    if (!music_filename) {
      res.status(400).json({ message: "Missing update music file" })
      return
    }

    if (!photo_filename) {
      res.status(400).json({ message: "Missing update music photo" })
      return
    }

    try {
      let a = await client.query(
        /* sql */ `
    select * from singer where singer_name = $1`,
        [req.body.singer_name],
      )

      if (a.rowCount == 0) {
        singerResult = await client.query(
          /* sql */ `
            insert into singer
            (singer_name)
            VALUES
            ($1) returning id
            `,
          [req.body.singer_name],
        )
        singerId = singerResult.rows[0].id
      } else {
        singerId = a.rows[0].id
      }
    } catch (error) {
      res.status(500).json({ message: "Post: Singer Database Error" })
      return
    }

    let music: InsertRow<Music> = {
      music_name: req.body.music_name,
      music_content: req.body.music_content,
      member_id: member.id,
      music_type: req.body.music_type,
      music_photo: null,
      music_file: null,
      singer_id: singerId,
      //singer_name: req.body.singer_name
    }
    try {
      await client.query(
        /* sql */ `
            insert into music
            (music_name,music_photo,music_content,music_file,music_type,member_id,singer_id)
            VALUES
            ($1,$2,$3,$4,$5,$6,$7) RETURNING id
            `,
        [
          music.music_name,
          photo_filename,
          music.music_content,
          music_filename,
          music.music_type,
          music.member_id,
          music.singer_id,
        ],
      )
    } catch (error) {
      res.status(500).json({ message: "Post: First Database Error" })
      return
    }

    //musicDataFile.useData
    //res.status(501).end('WIP')
    res.json({ message: "Created Music Card" })
  },
)

musicRouter.post("/music/:id/share", memberOnly, async (req, res) => {
  let member = req.session.user!

  if (!req.body.comment) {
    res.status(400).json({ message: "Please type comment" })
    return
  }

  // console.log(req.body.musicId)
  let post: InsertRow<Post> = {
    user_id: member.id,
    content: req.body.comment,
    reply_id: null,
    music_id: parseInt(req.body.musicId),
    singer_id: null,
  }
  try {
    await client.query(
      /* sql */ `
            insert into music_post
            (user_id,content,music_id)
            VALUES
            ($1,$2,$3) RETURNING id
            `,
      [post.user_id, post.content, post.music_id],
    )
  } catch (error) {
    res.status(500).json({ message: "Database Post Comment Error" })
    return
  }
  res.json({ message: "Post Comment Success" })
})

musicRouter.post("/singer/:id/comment", memberOnly, async (req, res) => {
  let member = req.session.user!

  if (!req.body.singercomment) {
    res.status(400).json({ message: "Please type comment" })
    return
  }

  let post = {
    user_id: member.id,
    content: req.body.singercomment,
    reply_id: null,
    singer_id: req.body.singerId,
  }
  try {
    await client.query(
      /* sql */ `
            insert into singer_post
            (user_id,content,singer_id)
            VALUES
            ($1,$2,$3) RETURNING id
            `,
      [post.user_id, post.content, post.singer_id],
    )
  } catch (error) {
    res.status(500).json({ message: "Database Post Comment Error" })
    return
  }
  res.json({ message: "Post Comment Success" })
})

musicRouter.post("/music/:id/like", memberOnly, async (req, res) => {
  let user_id = req.session.user?.id
  let music_id = req.params.id
  try {
    await client.query(
      "insert into music_like (music_id,user_id) values ($1,$2)",
      [music_id, user_id],
    )
    res.json({ ok: true })
  } catch (error: any) {
    console.error("Failed to post like:", error)
    res.status(500).json({ error: error.toString() })
  }
})

musicRouter.get("/music/:id/like", memberOnly, async (req, res) => {
  try {
    let user_id = req.session.user?.id
    let music_id = req.params.id
    let result = await client.query(
      "select count(*) as likes from music_like where music_id = $1 group by $2",
      [music_id, user_id],
    )
    res.json({ likes: result.rows[0].likes })
  } catch (error: any) {
    console.error("Failed to check like:", error)
    res.status(500).json({ error: error.toString() })
  }
})

musicRouter.get("/music/:id/session", async (req, res) => {
  try {
    let user_id = req.session.user?.id
    let music_id = req.params.id
    await client.query(
      "insert into user_log (music_id,user_id) values ($1,$2)",
      [music_id, user_id],
    )
    let result = await client.query(
      "select count(*) as views from user_log where music_id = $1",
      [music_id],
    )
    let views = result.rows[0].views
    res.json({ views: views })
  } catch (error) {
    res.status(500).json({ message: "GET: Database Error for view" })
  }
})

musicRouter.get("/singer/:id/session", async (req, res) => {
  try {
    let user_id = req.session.user?.id
    let singer_id = req.params.id
    await client.query(
      "insert into user_log (singer_id,user_id) values ($1,$2)",
      [singer_id, user_id],
    )
    let result = await client.query(
      "select count(*) as singerviews from user_log where singer_id = $1",
      [singer_id],
    )
    let singerviews = result.rows[0].singerviews
    res.json({ views: singerviews })
  } catch (error) {
    res.status(500).json({ message: "GET: Database Error for view" })
  }
})
// musicRouter.get('/music/singer', async (req,res) =>{
//   try{
//     let
//   }catch(error){

//   }
// })
