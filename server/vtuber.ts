import express from "express"
import { upload } from "../upload"
import fs from "fs"
import { adminOnly, memberOnly } from "../guard"
import { client } from "../db"
import { InsertRow, VTuber, VTuberGroup } from "../models"

export let vTuberRouter = express.Router()

vTuberRouter.get("/company", async (req, res) => {
  try {
    let result = await client.query("select * from vtuber_group order by id")
    res.json(result.rows)
  } catch (error: any) {
    res.status(500).json({ error: error.toString() })
  }
})

vTuberRouter.post(
  "/vtuber/update",
  adminOnly,
  upload.single("pic_file"),
  async (req, res) => {
    function cleanup() {
      if (req.file) {
        fs.unlink(req.file.path, (error) => {
          if (error) {
            console.error("Failed to cleanup vtuber photo:", error)
          }
        })
      }
    }
    let id = req.body.vtuber_id
    let {
      jpname,
      cnname,
      enname,
      vtuber_group_id,
      period,
      introduction,
      twitter,
      youtube,
      bilibili,
      label,
      recommend,
      url_3d,
    } = req.body
    let fields = ["jpname", "vtuber_group_id"]
    for (let field of fields) {
      if (!req.body[field]) {
        res.status(400).json({ message: "missing " + field })
        cleanup()
        return
      }
    }

    let images = req.file?.filename || req.body.pic_link
    if (!images) {
      res.status(400).json({ message: "missing pic_file or pic_link" })
      cleanup()
      return
    }
    // console.log(vtuber_group_id,
    //   period,
    //   images,
    //   introduction,
    //   twitter,
    //   youtube,
    //   bilibili,
    //   id)'

    try {
      // updata vtuber info
      await client.query(
        /* sql */ `
            update vtuber set
             vtuber_group_id = $1,
              period = $2,
              image = $3,
              introduction = $4,
              twitter = $5,
              youtube =$6,
              bilibili = $7,
              recommend = $8,
              url_3d = $9
            where id = $10
          `,
        [
          vtuber_group_id,
          period,
          images,
          introduction,
          twitter,
          youtube,
          bilibili,
          recommend || null,
          url_3d,
          id,
        ],
      )
      // console.log('update vtuber')
      await client.query(
        /* sql */ `
            update vtuber_name set name = $1 where lang_id = (select id from lang where lang = 'jp') and vtuber_id = $2
          `,
        [jpname, id],
      )
      if (cnname) {
        await client.query(
          /* sql */ `
              update vtuber_name set name = $1 where lang_id = (select id from lang where lang = 'cn') and vtuber_id = $2
            `,
          [cnname, id],
        )
      }
      if (enname) {
        await client.query(
          /* sql */ `
              update vtuber_name set name = $1 where lang_id = (select id from lang where lang = 'en') and vtuber_id = $2
            `,
          [enname, id],
        )
      }
      // console.log('updata lang');

      // insert tag
      await client.query("delete from vtuber_tag where vtuber_id = $1", [id])
      if (label) {
        let tagList = (label as string)
          .split(",")
          .map((tag) => tag.trim())
          .filter((tag) => tag.length <= 15 && tag.length >= 2)
          .slice(0, 5)
        for (let tag of tagList) {
          tag = tag.trim()
          if (tag) {
            let result = await client.query(
              /* sql */ `
                  select id from tag where tag_name = $1
                `,
              [tag],
            )
            let tag_id = result.rows[0]?.id
            if (!tag_id) {
              let result = await client.query(
                /* sql */ `
                    insert into tag (tag_name) values ($1) returning id
                  `,
                [tag],
              )
              tag_id = result.rows[0].id
            }

            await client.query(
              /* sql */ `
                  insert into vtuber_tag (vtuber_id, tag_id) values ($1,$2)
                `,
              [id, tag_id],
            )
          }
        }
      }
    } catch (error) {
      console.error("Failed to update vtuber:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }

    res.json({ message: "Update vtuber" })
  },
)

vTuberRouter.delete("/vtuber/:id", adminOnly, async (req, res) => {
  try {
    let id = req.params.id
    let result = await client.query(
      "delete from user_log where vtuber_id = $1",
      [id],
    )
    result = await client.query(
      "delete from favorite_vtuber where vtuber_id = $1",
      [id],
    )
    result = await client.query("delete from vtuber_tag where vtuber_id = $1", [
      id,
    ])
    // console.log(result)
    result = await client.query(
      "delete from vtuber_name where vtuber_id = $1",
      [id],
    )
    // console.log(result)
    result = await client.query("delete from vtuber where id = $1", [id])
    console.log(result)
    //   await client.query(
    //     /* sql */ `
    //   delete from vtuber_tag
    //   where vtuber_id = $1;
    //   delete from vtuber_name
    //   where vtuber_id = $1;
    //   delete from vtuber
    //   where vtuber_id = $1
    // `,
    //     [id],
    //   )
    res.json({ ok: true })
  } catch (error: any) {
    res.status(500).json({ error: error.toString() })
  }
})

/*** 有tag失敗 ***/
vTuberRouter.get("/vtuber/:id", async (req, res) => {
  try {
    let id = req.params.id
    let result = await client.query(
      /* sql */ `
        select
          vtuber.*
        , vtuber_group.group_name
        , (select vtuber_name.name
           from vtuber_name
           inner join lang on lang.id = vtuber_name.lang_id
           where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'jp'
          ) jp_vtuber_name
          , (select vtuber_name.name
            from vtuber_name
            inner join lang on lang.id = vtuber_name.lang_id
            where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'cn'
           ) cn_vtuber_name
          , (select vtuber_name.name
            from vtuber_name
            inner join lang on lang.id = vtuber_name.lang_id
            where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'en'
           ) en_vtuber_name
          
        from vtuber
        inner join vtuber_group
          on vtuber_group.id = vtuber_group_id
          where vtuber.id = $1 `,
      [id],
    )
    if (result.rowCount == 0) {
      res.status(404).json({ error: "user not found" })
    } else {
      let vtuber = result.rows[0]

      result = await client.query(
        /* sql */ `
        select
          tag_id
        , tag_name
        , vtuber_id
        from vtuber_tag
        inner join tag on tag.id = vtuber_tag.tag_id
        where vtuber_id = $1`,
        [id],
      )

      let tagList = result.rows
      // console.log(tagList)
      vtuber.tags = []

      /*** 有錯 ***/
      tagList.forEach((tag) => {
        vtuber.tags.push(tag.tag_name)
      })

      let sql = /* sql */ `
      select * from video where id in (
        select video_tag.video_id
        from video_tag
        inner join tag on tag.id = video_tag.tag_id
        where false
      `
      let bindings: string[] = []
      for (let i = 0; i < vtuber.tags.length; i++) {
        sql += ` or tag_name like $${i + 1}`
        bindings.push("%" + vtuber.tags[i] + "%")
      }
      sql += ") order by id desc"
      // console.log(sql, bindings)
      result = await client.query(sql, bindings)
      vtuber.video_list = result.rows

      // vtuberList.forEach((vtuber) => {
      //   vtuber.tags = vtuber.tags.join(", ")
      // })

      res.json({ vtuber })
    }
  } catch (error: any) {
    res.status(500).json({ error: error.toString() })
  }
})

vTuberRouter.get("/vtuber_group", async (req, res) => {
  try {
    let result = await client.query(/* sql */ `
        select
          vtuber.id
        , vtuber.image as img
        , vtuber_group.group_name as group
        , vtuber_group.group_image
        , (select vtuber_name.name
           from vtuber_name
           inner join lang on lang.id = vtuber_name.lang_id
           where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'jp'
          ) as jpname
          , (select vtuber_name.name
            from vtuber_name
            inner join lang on lang.id = vtuber_name.lang_id
            where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'cn'
           ) as name
          
        from vtuber
        inner join vtuber_group
          on vtuber_group.id = vtuber_group_id
          order by id
      `)

    let list = result.rows
    // console.log(list)
    res.json(list)
  } catch (error: any) {
    res.status(500).json({ error: error.toString() })
  }
})

vTuberRouter.get("/vtuber", async (req, res) => {
  try {
    let result = await client.query(/* sql */ `
        select
          vtuber.*
        , vtuber_group.group_name
        , (select vtuber_name.name
           from vtuber_name
           inner join lang on lang.id = vtuber_name.lang_id
           where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'jp'
          ) jp_vtuber_name
          , (select vtuber_name.name
            from vtuber_name
            inner join lang on lang.id = vtuber_name.lang_id
            where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'cn'
           ) cn_vtuber_name
          , (select vtuber_name.name
            from vtuber_name
            inner join lang on lang.id = vtuber_name.lang_id
            where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'en'
           ) en_vtuber_name
          
        from vtuber
        inner join vtuber_group
          on vtuber_group.id = vtuber_group_id
          order by id
      `)

    let vtuberList = result.rows

    result = await client.query(/* sql */ `
        select
          tag_id
        , tag_name
        , vtuber_id
        from vtuber_tag
        inner join tag on tag.id = vtuber_tag.tag_id
    `)

    let tagList = result.rows

    vtuberList.forEach((vtuber) => ((vtuber.tags = []), (vtuber.keyword = [])))

    tagList.forEach((tag) => {
      let vtuber = vtuberList.find((vtuber) => vtuber.id == tag.vtuber_id)
      vtuber.tags.push(tag.tag_name)
      vtuber.keyword.push(tag.tag_name)
    })

    vtuberList.forEach((vtuber) => {
      vtuber.tags = vtuber.tags.join(", ")
    })

    // console.log(vtuberList)
    // console.log(tagList)
    res.json(vtuberList)
  } catch (error: any) {
    res.status(500).json({ error: error.toString() })
  }
})

/*** pic_file 是否會撞名***/
vTuberRouter.post(
  "/vtuber",
  adminOnly,
  upload.single("pic_file"),
  async (req, res) => {
    // console.log("post vtuber body:", req.body)
    // console.log("post vtuber file:", req.file)

    // let user = req.session.user!
    function cleanup() {
      if (req.file) {
        fs.unlink(req.file.path, (error) => {
          if (error) {
            console.error("Failed to cleanup vtuber photo:", error)
          }
        })
      }
    }
    let {
      jpname,
      cnname,
      enname,
      vtuber_group_id,
      period,
      introduction,
      twitter,
      youtube,
      bilibili,
      label,
    } = req.body
    let fields = ["jpname", "vtuber_group_id"]
    for (let field of fields) {
      if (!req.body[field]) {
        res.status(400).json({ message: "missing " + field })
        cleanup()
        return
      }
    }

    let images = req.file?.filename || req.body.pic_link
    if (!images) {
      res.status(400).json({ message: "missing pic_file or pic_link" })
      cleanup()
      return
    }
    // try {
    //   //find name
    //   let result = await client.query(
    //     "select name from vtuber_name where name = $1 or name = $2 or name = $3",
    //     [jpname, cnname, enname],
    //   )
    //   if (result.rowCount != 0) {
    //     res.status(409).json({ message: "duplicated content" })
    //     return
    //   }
    // } catch (error) {
    //   console.error("Failed to check vtuber name duplication:", error)
    //   res.status(500).json({ message: "Database Error" })
    //   return
    // }

    let vtuber: InsertRow<VTuber> = {
      vtuber_group_id,
      period,
      images,
      introduction,
      twitter,
      youtube,
      bilibili,
    }
    console.log("insert vtuber", vtuber)

    try {
      // insert vtuber info
      let result = await client.query(
        /* sql */ `
        insert into vtuber
        ( vtuber_group_id,
          period,
          image,
          introduction,
          twitter,
          youtube,
          bilibili )
        values
        ($1, $2, $3, $4, $5, $6, $7)
        returning id
      `,
        [
          vtuber_group_id,
          period,
          images,
          introduction,
          twitter,
          youtube,
          bilibili,
        ],
      )
      let vtuber_id = result.rows[0].id

      await client.query(
        /* sql */ `
        insert into vtuber_name (vtuber_id, name, lang_id) values (
          $1, $2,
          (select id from lang where lang = 'jp')
        )
      `,
        [vtuber_id, jpname],
      )
      if (cnname) {
        await client.query(
          /* sql */ `
          insert into vtuber_name (vtuber_id, name, lang_id) values (
            $1, $2,
            (select id from lang where lang = 'cn')
          )
        `,
          [vtuber_id, cnname],
        )
      }
      if (enname) {
        await client.query(
          /* sql */ `
          insert into vtuber_name (vtuber_id, name, lang_id) values (
            $1, $2,
            (select id from lang where lang = 'en')
          )
        `,
          [vtuber_id, enname],
        )
      }

      // insert tag
      if (label) {
        let tagList = (label as string)
          .split(",")
          .map((tag) => tag.trim())
          .filter((tag) => tag.length <= 15 && tag.length >= 2)
          .slice(0, 5)
        for (let tag of tagList) {
          tag = tag.trim()
          if (tag) {
            let result = await client.query(
              /* sql */ `
              select id from tag where tag_name = $1
            `,
              [tag],
            )
            let tag_id = result.rows[0]?.id
            if (!tag_id) {
              let result = await client.query(
                /* sql */ `
                insert into tag (tag_name) values ($1) returning id
              `,
                [tag],
              )
              tag_id = result.rows[0].id
            }

            await client.query(
              /* sql */ `
              insert into vtuber_tag (vtuber_id, tag_id) values ($1,$2)
            `,
              [vtuber_id, tag_id],
            )
          }
        }
      }
    } catch (error) {
      console.error("Failed to insert vtuber:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }

    res.json({ message: "Created vtuber" })
  },
)

vTuberRouter.get("/admin/vtuber", adminOnly, async (req, res) => {
  try {
    let search = req.query.q as any
    let id = +search
    let where = ""
    if (id) {
      // search by id
      where = /* sql */ `where vtuber.id = $1`
      search = id
    } else {
      // search by keyword
      where = /* sql */ `
       where vtuber_group.group_name like $1  or (select vtuber_name.name
        from vtuber_name
        inner join lang on lang.id = vtuber_name.lang_id
        where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'jp'
       ) like $1 or (select vtuber_name.name
        from vtuber_name
        inner join lang on lang.id = vtuber_name.lang_id
        where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'cn'
       )  like $1
      `
      search = "%" + search + "%"
    }

    let result = await client.query(
      /* sql */ `
      select
      vtuber.*
      , vtuber_group.group_name
      , (select vtuber_name.name
         from vtuber_name
         inner join lang on lang.id = vtuber_name.lang_id
         where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'jp'
        ) as jp_vtuber_name
        , (select vtuber_name.name
          from vtuber_name
          inner join lang on lang.id = vtuber_name.lang_id
          where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'cn'
         ) as cn_vtuber_name
        , (select vtuber_name.name
          from vtuber_name
          inner join lang on lang.id = vtuber_name.lang_id
          where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'en'
         ) as en_vtuber_name
      from vtuber
      inner join vtuber_group
        on vtuber_group.id = vtuber_group_id
      ${where}
      order by id
    `,
      [search],
    )

    let vtuberList = result.rows

    result = await client.query(/* sql */ `
    select
      tag_id
    , tag_name
    , vtuber_id
    from vtuber_tag
    inner join tag on tag.id = vtuber_tag.tag_id
    where false ${vtuberList
      .map((vtuber) => ` or vtuber_id = ` + vtuber.id)
      .join("")}
`)

    let tagList = result.rows
    vtuberList.forEach((vtuber) => ((vtuber.tags = []), (vtuber.keyword = [])))

    tagList.forEach((tag) => {
      let vtuber = vtuberList.find((vtuber) => vtuber.id == tag.vtuber_id)
      vtuber.tags.push(tag.tag_name)
      vtuber.keyword.push(tag.tag_name)
    })

    vtuberList.forEach((vtuber) => {
      vtuber.tags = vtuber.tags.join(", ")
    })

    // console.log(vtuberList)
    res.json(vtuberList)
  } catch (error: any) {
    // console.log(error)
    res.status(500).json({ error: error.toString() })
  }
})

vTuberRouter.post(
  "/group",
  adminOnly,
  upload.single("pic_file"),
  async (req, res) => {
    function cleanup() {
      if (req.file) {
        fs.unlink(req.file.path, (error) => {
          if (error) {
            console.error("Failed to cleanup group photo:", error)
          }
        })
      }
    }
    let { group_name } = req.body
    let fields = ["group_name"]
    for (let field of fields) {
      if (!req.body[field]) {
        res.status(400).json({ message: "missing " + field })
        cleanup()
        return
      }
    }

    let images = req.file?.filename || req.body.pic_link
    if (!images) {
      res.status(400).json({ message: "missing pic_file or pic_link" })
      cleanup()
      return
    }

    let group: InsertRow<VTuberGroup> = {
      group_name,
      group_image: images,
    }
    console.log("insert group", group)

    try {
      // insert vtuber info
      await client.query(
        /* sql */ `
        insert into vtuber_group
        ( group_name , group_image )
        values
        ($1, $2)
      `,
        [group_name, images],
      )
    } catch (error) {
      console.error("Failed to insert Group:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }

    res.json({ message: "Created Group" })
  },
)

vTuberRouter.post(
  "/group/update",
  adminOnly,
  upload.single("pic_file"),
  async (req, res) => {
    function cleanup() {
      if (req.file) {
        fs.unlink(req.file.path, (error) => {
          if (error) {
            console.error("Failed to cleanup group photo:", error)
          }
        })
      }
    }
    let id = req.body.group_id
    let { group_name } = req.body
    let fields = ["group_name"]
    for (let field of fields) {
      if (!req.body[field]) {
        res.status(400).json({ message: "missing " + field })
        cleanup()
        return
      }
    }

    let images = req.file?.filename || req.body.pic_link
    if (!images) {
      res.status(400).json({ message: "missing pic_file or pic_link" })
      cleanup()
      return
    }
    try {
      await client.query(
        /* sql */ `
            update vtuber_group set
            group_name = $1,
            group_image = $2
            where id = $3
          `,
        [group_name, images, id],
      )
    } catch (error) {
      console.error("Failed to update group:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }

    res.json({ message: "Update group" })
  },
)

vTuberRouter.delete("/group/:id", adminOnly, async (req, res) => {
  try {
    let id = req.params.id
    await client.query("delete from vtuber_group where id = $1", [id])
    res.json({ ok: true })
  } catch (error: any) {
    res.status(500).json({ error: error.toString() })
  }
})

vTuberRouter.delete("/vtuber/:id/like", memberOnly, async (req, res) => {
  let user_id = req.session.user?.id
  let vtuber_id = req.params.id
  await client.query(
    "delete from favorite_vtuber where user_id = $1 and vtuber_id = $2",
    [user_id, vtuber_id],
  )
  res.json({ ok: true })
})
vTuberRouter.post("/vtuber/:id/like", memberOnly, async (req, res) => {
  let user_id = req.session.user?.id
  let vtuber_id = req.params.id
  try {
    //find name
    let result = await client.query(
      "select * from favorite_vtuber where user_id = $1 and vtuber_id = $2",
      [user_id, vtuber_id],
    )
    if (result.rowCount == 0) {
      await client.query(
        "insert into favorite_vtuber (user_id,vtuber_id) values ($1,$2)",
        [user_id, vtuber_id],
      )
    }

    //  result = await client.query(
    //   "select count(*) as likes from favorite_vtuber where vtuber_id = $1",
    //   [vtuber_id],
    // )
    // res.json({ likes: result.rows[0].likes })

    res.json({ ok: true })
  } catch (error: any) {
    console.error("Failed to post like:", error)
    res.status(500).json({ error: error.toString() })
  }
})

vTuberRouter.get("/vtuber/:id/like", memberOnly, async (req, res) => {
  try {
    let user_id = req.session.user?.id
    let vtuber_id = req.params.id
    let result = await client.query(
      "select * from favorite_vtuber where user_id = $1 and vtuber_id = $2",
      [user_id, vtuber_id],
    )
    let hasLiked: boolean = result.rowCount > 0
    res.json({ hasLiked })
  } catch (error: any) {
    console.error("Failed to check like:", error)
    res.status(500).json({ error: error.toString() })
  }
})

vTuberRouter.get("/user/:id/vtuber", async (req, res) => {
  try {
    let user_id = req.params.id
    let result = await client.query(
      "select count(*) from favorite_vtuber where user_id = $1",
      [user_id],
    )
    let vtuber = result.rows[0]
    res.json({ like: vtuber })
  } catch (error: any) {
    console.error("Failed to check like:", error)
    res.status(500).json({ error: error.toString() })
  }
})

vTuberRouter.get("/user/:id/favorite", async (req, res) => {
  try {
    let user_id = req.params.id
    let result = await client.query(
      /* sql */ `select image, vtuber.id,
      (select vtuber_name.name
        from vtuber_name
        inner join lang on lang.id = vtuber_name.lang_id
        where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'jp'
       ) jpname
      from vtuber 
      inner join favorite_vtuber on vtuber_id = vtuber.id
      inner join member on member.id = user_id
      where user_id = $1`,
      [user_id],
    )
    let vtuber = result.rows
    res.json({ vtuber })
  } catch (error: any) {
    console.error("Failed to check like:", error)
    res.status(500).json({ error: error.toString() })
  }
})

vTuberRouter.get("/home/vtuber", async (req, res) => {
  try {
    let result =
      await client.query(/* sql */ `select image, id, (select vtuber_name.name
           from vtuber_name
           inner join lang on lang.id = vtuber_name.lang_id
           where vtuber_name.vtuber_id = vtuber.id and lang.lang = 'jp'
          ) jpname
      from vtuber
      where recommend > 0
      order by recommend`)
    let vtuber = result.rows
    res.json({ vtuber })
  } catch (error: any) {
    console.error("Failed to check like:", error)
    res.status(500).json({ error: error.toString() })
  }
})
