import express from "express"
import { upload } from "../upload"
import fs from "fs"
import { client } from "../db"
import { memberOnly,adminOnly } from "../guard"
import { InsertRow, Video } from "../models"

export let videoRouter = express.Router()

videoRouter.get("/category", async (req, res) => {
  try {
    let result = await client.query("select * from category")
    res.json(result.rows)
  } catch (error: any) {
    res.status(500).json({ error: error.toString() })
  }
})
videoRouter.get("/category/:id", async (req, res) => {
  let id = req.params.id
  try {
    let result = await client.query(
      /* sql */ `
        select
          video.*
        , category.category
        , post.content
        , username , icon
        from video
        inner join category
          on category.id = category_id
        inner join video_post
          on video_id = video.id
        inner join post
          on post.id = post_id
        inner join member
          on member.id = user_id
          where category.id = $1
          order by id
      `,
      [id],
    )

    let videoCategory = result.rows
    res.json(videoCategory)
  } catch (error: any) {
    res.status(500).json({ error: error.toString() })
  }
})

videoRouter.post(
  "/video",
  memberOnly,
  upload.single("pic_file"),
  async (req, res) => {
    // console.log("post vtuber body:", req.body)
    // console.log("post vtuber file:", req.file)

    // let user = req.session.user!
    function cleanup() {
      if (req.file) {
        fs.unlink(req.file.path, (error) => {
          if (error) {
            console.error("Failed to cleanup vtuber photo:", error)
          }
        })
      }
    }
    let { category_id, title, pic_link, info, videourl, label } = req.body
    let fields = ["title", "videourl"]
    for (let field of fields) {
      if (!req.body[field]) {
        res.status(400).json({ message: "missing " + field })
        cleanup()
        return
      }
    }
    let youtube = videourl.includes("youtube.com")
    if (!youtube) {
      res.status(409).json({ message: "error url" })
      cleanup()
      return
    }

    let images = req.file?.filename || req.body.pic_link
    let user_id = req.session.user?.id
    let video: InsertRow<Video> = {
      title,
      image: pic_link,
      video_url: videourl,
    }
    console.log("insert video", video)
    if (videourl) {
      videourl = videourl.substr(videourl.indexOf("=") + 1)
    }
    if (videourl.length < 11){
      res.status(409).json({ message: "error url" })
      cleanup()
      return
    }
    try {
      //find name
      let result = await client.query(
        "select video_url from video where video_url like $1",
        ["%" + videourl + "%"],
      )
      if (result.rowCount != 0) {
        res.status(409).json({ message: "duplicated content" })
        cleanup()
        return
      }
    } catch (error) {
      console.error("Failed to check video url duplication:", error)
      res.status(500).json({ message: "Database Error" })
      cleanup()
      return
    }
    try {
      // insert vtuber info
      let result = await client.query(
        /* sql */ `
          insert into video
          ( category_id,
            title,
            image,
            video_url )
          values
          ($1, $2, $3, $4)
          returning id
        `,
        [category_id, title, images, videourl],
      )
      let video_id = result.rows[0].id

      result = await client.query(
        /* sql */ `
          insert into post (content, user_id) values 
          ($1, $2) returning id
        `,
        [info, user_id],
      )
      let post_id = result.rows[0].id
      await client.query(
        /* sql */ `
          insert into video_post (video_id, post_id) values (
            $1, $2
          )
        `,
        [video_id, post_id],
      )

      // insert tag
      if (label) {
        let tagList = (label as string)
          .split(",")
          .map((tag) => tag.trim())
          .filter((tag) => tag.length <= 15 && tag.length >= 2)
          .slice(0, 5)
        for (let tag of tagList) {
          tag = tag.trim()
          if (tag) {
            let result = await client.query(
              /* sql */ `
                select id from tag where tag_name = $1
              `,
              [tag],
            )
            let tag_id = result.rows[0]?.id
            if (!tag_id) {
              let result = await client.query(
                /* sql */ `
                  insert into tag (tag_name) values ($1) returning id
                `,
                [tag],
              )
              tag_id = result.rows[0].id
            }

            await client.query(
              /* sql */ `
                insert into video_tag (video_id, tag_id) values ($1,$2)
              `,
              [video_id, tag_id],
            )
          }
        }
      }
    } catch (error) {
      console.error("Failed to insert video:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }

    res.json({ message: "Created video" })
  },
)

videoRouter.get("/video", async (req, res) => {
  try {
    let page = +req.query.page! || 1
    let count = +req.query.count! || 8
    let cat = +req.query.cat!
    let word = req.query.word || ''
    page--
    let binding: string[] = []
    if (word) {
      binding.push("%" + word + "%")
    }
    let result = await client.query(
      /* sql */ `
        select
          video.*
        , category.category
        , post.content
        , username
        , icon
        , user_id
        , (select count(*) from user_log where video_id = video.id) as count
        from video
        inner join category
          on category.id = category_id
        inner join video_post
          on video_id = video.id
        inner join post
          on post.id = post_id
        inner join member
          on member.id = user_id
        where true
        ${cat ? " and video.category_id = " + cat : ""}
        ${word ? " and video.title like $1" : ""}
        order by id desc
        LIMIT ${count} OFFSET ${page * count};
      `,
      binding,
    )
    // LIMIT 5 OFFSET 3;
    let videoList = result.rows
    result = await client.query(/* sql */ `
        select
          tag_id
        , tag_name
        , video_id
        from video_tag
        inner join tag on tag.id = video_tag.tag_id
        where false ${videoList
          .map((video) => ` or video_id = ` + video.id)
          .join("")}
    `)

    let tagList = result.rows

    videoList.forEach((video) => ((video.tags = []), (video.keyword = [])))

    tagList.forEach((tag) => {
      let video = videoList.find((video) => video.id == tag.video_id)
      video.tags.push(tag.tag_name)
      video.keyword.push(tag.tag_name)
    })

    videoList.forEach((video) => {
      video.tags = video.tags.join(", ")
    })

    result = await client.query(
      /* sql */ `
        select
          video.*
        , category.category
        , post.content
        , username
        , icon
        , user_id
        , (select count(*) from user_log where video_id = video.id) as count
        from video
        inner join category
          on category.id = category_id
        inner join video_post
          on video_id = video.id
        inner join post
          on post.id = post_id
        inner join member
          on member.id = user_id
        where true
        ${cat ? " and video.category_id = " + cat : ""}
        ${word ? " and video.title like $1" : ""}
        order by id desc;
      `,
      binding,
    )

    let videoTotal = result.rows.length
    // result = await client.query(
    //   /* sql */ `
    //     select
    //       count(*) as count,
    //       video_id
    //     from user_log
    //     where video_id IS NOT NULL
    //     GROUP BY video_id
    // `,
    // )
    // let countList = result.rows
    // countList.forEach((count) => {
    //   let video = videoList.find((video) => video.id == count.video_id)
    //   video.count = count.count
    // })

    // console.log(vtuberList)
    // console.log(tagList)
    res.json({videoList,videoTotal})
  } catch (error: any) {
    res.status(500).json({ error: error.toString() })
  }
})

videoRouter.get("/video/:id", async (req, res) => {
  try {
    let id = req.params.id
    let user_id = req.session.user?.id

    let result = await client.query(
      /* sql */ `
        select
          video.*
        , category.category
        , post.content
        , username
        , icon
        ,user_id
        from video
        inner join category
          on category.id = category_id
        inner join video_post
          on video_id = video.id
        inner join post
          on post.id = post_id
        inner join member
          on member.id = user_id
          where video.id = $1
          order by id
      `,
      [id],
    )
    if (result.rowCount == 0) {
      res.status(404).json({ error: "user not found" })
    } else {
      await client.query(
        "insert into user_log (user_id,video_id) values ($1,$2)",
        [user_id, id],
      )
      let video = result.rows[0]

      result = await client.query(
        /* sql */ `
        select
          tag_id
        , tag_name
        , video_id
        from video_tag
        inner join tag on tag.id = video_tag.tag_id
        where video_id = $1
    `,
        [id],
      )

      let tagList = result.rows

      video.tags = []
      video.keyword = []

      tagList.forEach((tag) => {
        video.tags.push(tag.tag_name)
        video.keyword.push(tag.tag_name)
      })
      video.tags = video.tags.join(", ")

      result = await client.query(
        /* sql */ `
        select
          count(*) as count
        from user_log
        where video_id = $1
    `,
        [id],
      )
      let count = result.rows[0].count
      video.count = count

      // console.log(vtuberList)
      // console.log(tagList)
      res.json({ video })
    }
  } catch (error: any) {
    res.status(500).json({ error: error.toString() })
  }
})

videoRouter.get("/home/video", async (req, res) => {
  try {
    let result =
      await client.query(/* sql */ `select title, id,image, (select count(*) from user_log where user_log.video_id = video.id) count
      from video
      order by (select count(*) from user_log where user_log.video_id = video.id) desc
      limit 10`)
    let vtuber = result.rows
    res.json(vtuber)
  } catch (error: any) {
    console.error("Failed to check like:", error)
    res.status(500).json({ error: error.toString() })
  }
})


videoRouter.post(
  "/video/message/:id",
  memberOnly,
  async (req, res) => {
    try {
      let id = req.params.id
      let user_id = req.session.user?.id
      let { content } = req.body
      await client.query(
        /* sql */ `
        insert into post
        ( user_id,
          reply_id,
          content )
        values
        ($1, $2, $3)
        returning id
      `,
      [user_id, id, content],
    )

      res.json({ message: "Created message" })

    } catch (error: any) {
      res.status(500).json({ error: error.toString() })
    }
  })


  videoRouter.get("/video/:id/content", async (req, res) => {
    try {
      let video_id = req.params.id
      let result = await client.query(/*SQL*/
        `select content, member.id , member.icon , member.username ,post.id as post
        from post 
        inner join member
          on member.id = user_id
        where reply_id = $1
        order by post.id desc`,
        [video_id],
      )
      let content = result.rows
      res.json( content )
    } catch (error: any) {
      console.error("Failed to get content:", error)
      res.status(500).json({ error: error.toString() })
    }
  })


  videoRouter.delete("/video/:id/like", memberOnly, async (req, res) => {
    let user_id = req.session.user?.id
    let video_id = req.params.id
    await client.query(
      "delete from favorite_video where user_id = $1 and video_id = $2",
      [user_id, video_id],
    )
    res.json({ ok: true })
  })
  videoRouter.post("/video/:id/like", memberOnly, async (req, res) => {
    let user_id = req.session.user?.id
    let video_id = req.params.id
    try {
      //find name
      let result = await client.query(
        "select * from favorite_video where user_id = $1 and video_id = $2",
        [user_id, video_id],
      )
      if (result.rowCount == 0) {
        await client.query(
          "insert into favorite_video (user_id,video_id) values ($1,$2)",
          [user_id, video_id],
        )
      }
  
      // res.json({ likes: result.rows[0].likes })
  
      res.json({ ok: true })
    } catch (error: any) {
      console.error("Failed to post like:", error)
      res.status(500).json({ error: error.toString() })
    }
  })
  
  videoRouter.get("/video/:id/like", memberOnly, async (req, res) => {
    try {
      let user_id = req.session.user?.id
      let video_id = req.params.id
      let result = await client.query(
        "select * from favorite_video where user_id = $1 and video_id = $2",
        [user_id, video_id],
      )
      let hasLiked: boolean = result.rowCount > 0

      result = await client.query(
        "select count(*) as likes from favorite_video where video_id = $1",
        [video_id],
      )
      res.json({ hasLiked ,likes: result.rows[0].likes})
    } catch (error: any) {
      console.error("Failed to check like:", error)
      res.status(500).json({ error: error.toString() })
    }
  })


  videoRouter.get("/video/:id/favorite", async (req, res) => {
    try {
      let user_id = req.params.id
      let result = await client.query(
        /* sql */ `select video.id, image, title
        from video 
        inner join favorite_video on video_id = video.id
        inner join member on member.id = user_id
        where user_id = $1`,
        [user_id],
      )
      let video = result.rows
      res.json({ video })
    } catch (error: any) {
      console.error("Failed to favorite video:", error)
      res.status(500).json({ error: error.toString() })
    }
  })

  /*研究中*/
  videoRouter.delete("/video/:id", adminOnly, async (req, res) => {
    try {
      let id = req.params.id
      let result = await client.query(
        "delete from user_log where video_id = $1",
        [id],
      )
      result = await client.query(
        "delete from favorite_video where video_id = $1",
        [id],
      )
      result = await client.query("delete from video_tag where video_id = $1", [
        id,
      ])
      // result = await client.query("delete from post inner join video_post on post_id = post.id where video_id = $1", [
      //   id,
      // ])
      result = await client.query("delete from video_post where video_id = $1", [
        id,
      ])
      result = await client.query("delete from video_tag where video_id = $1", [
        id,
      ])
      // console.log(result)
      result = await client.query("delete from video where id = $1", [id])
      console.log(result)

      res.json({ ok: true })
    } catch (error: any) {
      res.status(500).json({ error: error.toString() })
    }
  })


  videoRouter.delete("/post/:id", adminOnly, async (req, res) => {
    try {
      let id = req.params.id
      let result = await client.query(
        "delete from post where id = $1", [id])
      console.log(result)

      res.json({ ok: true })
    } catch (error: any) {
      res.status(500).json({ error: error.toString() })
    }
  })