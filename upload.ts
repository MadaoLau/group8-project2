import multer from "multer";

let fileCounter = 0  //防止同時間上傳撞名
const storage = multer.diskStorage({
	destination: './uploads',
	filename: function (req, file, cb) {
		fileCounter++
		let ext: string = file.mimetype.split('/').pop()!
		ext = ext.split('-').pop()!
		ext = ext.split(';')[0]
		if (file.fieldname == 'video_file') {
            switch (ext) {
                //TODO: add all mimetype of photo
                case 'mp4':
                    cb(null, `${file.fieldname}-${Date.now()}-${fileCounter}.${ext}`);
                    break
                default:
                    req.res?.status(400).json({ message: 'Invalid video format'})
                    cb(new Error('invalid video format'), '')
                    break
            }
        } else if (file.fieldname) {
            switch (ext) {
                //TODO: add all mimetype of photo
                case 'jpeg':
                case 'png':
                case 'gif':
                    cb(null, `${file.fieldname}-${Date.now()}-${fileCounter}.${ext}`);
                    break
                default:
                    req.res?.status(400).json({ message: 'Invalid image format'})
                    cb(new Error('invalid image format'), '')
                    break
            }
		} else {
				req.res?.status(400).json({ message: 'Invalid field'})
				cb(new Error('invalid field'), '')
			}
	}
})
export let upload = multer({ storage }) //自動建資料夾