import express from "express"
import path from "path"
import { client } from "./db"
import multer from "multer"
//import fs from "fs"
// import { DataFile } from "./data-file"
import { memberOnly } from "./guard"
import { Request, Response } from 'express';

export let artsRouter = express.Router()


const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve('./uploads'));
  },
  filename: function (req, file, cb) {

    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
})
const upload = multer({ storage })


//InsertRow (Omit id)

export type Arts = {

  arts_name: string
  arts_photo: string | null
  arts_content: string
  artist: string


}

export type User = {
  id: number
  is_admin: boolean
  username: string
  password: string
  email: string
}

artsRouter.post('/artsSubmit', memberOnly, upload.single("arts_photo"), async (req: Request, res: Response) => {
  // const content = req.body.content


  // if (filename) {
  //     await client.query(`insert into arts ()`)
  // } else {
  //     await client.query(``)
  // }
  let member = req.session.user!
  console.log(member)

  let filename = req.file?.filename



  let arts_name = req.body.arts_name
  let arts_content = req.body.arts_content
  let artist = req.body.artist_name


  const artsItem = await client.query(`insert into arts(title, upfilename,intro,artist) values ($1,$2,$3,$4)`, [arts_name, filename, arts_content, artist])
  console.log(artsItem)
  //res.redirect('/cinema.html')
  res.json({ success: true })

})

artsRouter.get("/arts", async (req: Request, res: Response) => {
  const arts = await client.query(`select * from arts`)
  console.log(req.session.user)
  res.json(arts.rows)
})

artsRouter.get("/arts/:id", async (req: Request, res: Response) => {
  try {

    const id = req.params.id
    const result = await client.query(`select * from arts where id = $1`, [id])
    const art = result.rows[0]
    res.json({ art })
  }
  catch (error: any) {
    res.json({ error: error.toString() })
  }

})

artsRouter.get("/arts_user", memberOnly, async (req: Request, res: Response) => {
  try {
    const result = await client.query(`select * from arts where member_id = $1`, [req.session.user!.id])
    res.json(result.rows)

  }
  catch {
    
  }
  

})

// artsRouter.get("/arts", async (req:Request, res: Response) => {
//   let id = req.query.id
//   const art = await client.query(`select * from arts where id = ${req.params.id}`)
//   res.json(art.rows[0])
// })








