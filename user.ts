import { client } from "./db"
import path from "path"
import { Application } from "express"
import { adminOnly, memberOnly, visitor } from "./guard"
import { User, Register } from "./models"
import "./session"
import { hashPassword, checkPassword } from "./bcrypt"
import { upload } from "./upload"
import fs from "fs"

export function attachUserRoutes(app: Application) {
  app.get("/login.html", visitor, (req, res) => {
    res.sendFile(path.resolve(path.join("public", "login.html")))
  })
  app.get("/user-password.html", memberOnly, (req, res) => {
    res.sendFile(path.resolve(path.join("public", "user-password.html")))
  })

  app.post("/logout", (req, res) => {
    req.session.user = undefined
    req.session.save()
    res.json({ message: "Logout Success" })
  })

  //login  /** ID 跟 Username 撞**/
  app.post("/login", async (req, res) => {
    if (!req.body.username || typeof req.body.username != "string") {
      res.status(400).json({ message: "Missing username string in req.body" })
      return
    }
    if (!req.body.password || typeof req.body.password != "string") {
      res.status(400).json({ message: "Missing password string in req.body" })
      return
    }
    const username = req.body.username
    let rows: Pick<
      User,
      "id" | "username" | "is_admin" | "icon" | "email" | "password"
    >[]
    try {
      var re = /^[0-9]+.?[0-9]*/
      if (re.test(username)) {
        let result = await client.query(
          "select * from member where username = $1 or id = $2",
          [username, username],
        )
        rows = result.rows
      } else {
        let result = await client.query(
          "select * from member where username = $1 or email = $2",
          [username, username],
        )
        rows = result.rows
      }
    } catch (error: any) {
      console.error("Failed to get user for login:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }

    if (rows.length == 0) {
      res.status(400).json({
        message: "This username is not registered. Maybe wrong username?",
      })
      return
    }
    let user = rows[0]

    if (!(await checkPassword(req.body.password, user.password))) {
      res.status(400).json({
        message: "Wrong username or password",
      })
      return
    }
    req.session.user = {
      login: true,
      id: user.id,
      username: user.username,
      is_admin: user.is_admin,
      icon: user.icon,
    }
    req.session.save()
    res.json({ message: "Login Success" })
    return
  })

  app.post("/user/password", memberOnly, async (req, res) => {
    let user_id = req.session.user!.id
    let username = req.session.user!.username
    let { newpassword, password, confirmpassword } = req.body
    let fields = ["newpassword", "password", "confirmpassword"]
    for (let field of fields) {
      if (!req.body[field]) {
        res.status(400).json({ message: "missing " + field })
        return
      }
    }
    if (newpassword.length < 6 || newpassword.length > 20) {
      res.status(400).json({
        message:
          "password must be at least 6 characters or at most 20 characters",
      })
      return
    }
    if (newpassword != confirmpassword) {
      res.status(400).json({ message: "password not match" })
      return
    }
    if (newpassword == username) {
      res
        .status(400)
        .json({ message: "Password must be different from username" })
      return
    }
    if (password == newpassword) {
      res
        .status(400)
        .json({ message: "Password must be different from old Password" })
      return
    }
    let re = /[0-9]/
    if (!re.test(newpassword)) {
      res
        .status(400)
        .json({ message: "Password must contain at least one number (0 to 9)" })
      return
    }
    re = /[a-z]/
    if (!re.test(newpassword)) {
      res.status(400).json({
        message: "Password must contain at least one lowercase letter (a-z)",
      })
      return
    }
    re = /[A-Z]/
    if (!re.test(newpassword)) {
      res.status(400).json({
        message: "Password must contain at least one uppercase letter (A-Z)",
      })
      return
    }
    let rows: Pick<
      User,
      "id" | "username" | "is_admin" | "icon" | "email" | "password"
    >[]
    try {
      //find name

      let result = await client.query("select * from member where id = $1", [
        user_id,
      ])
      rows = result.rows

      if ((result.rowCount = 0)) {
        res.status(409).json({ message: "Can not find user" })
        return
      }
    } catch (error) {
      console.error("Failed to check user :", error)
      res.status(500).json({ message: "Database Error" })
      return
    }
    if (rows.length == 0) {
      res.status(400).json({
        message: "This username is not registered. Maybe wrong username?",
      })
      return
    }
    let user = rows[0]

    if (!(await checkPassword(req.body.password, user.password))) {
      res.status(400).json({
        message: "Wrong old password",
      })
      return
    }

    let new_password = await hashPassword(newpassword)
    try {
      await client.query("Update member set password = $1 WHERE id = $2", [
        new_password,
        user_id,
      ])
    } catch (error) {
      console.error("Failed to updata member:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }

    res.json({ message: "Change password" })
  })

  app.post("/register", async (req, res) => {
    let { email, username, password, confirmpassword, is_admin } = req.body
    let fields = ["username", "password", "confirmpassword"]
    for (let field of fields) {
      if (!req.body[field]) {
        res.status(400).json({ message: "missing " + field })
        return
      }
    }
    if (!req.session.user?.is_admin) {
      fields = ["email"]
      for (let field of fields) {
        if (!req.body[field]) {
          res.status(400).json({ message: "missing " + field })
          return
        }
      }
    }
    if (!is_admin) {
      is_admin = false
    }
    if (!req.session.user?.is_admin) {
      if (req.body.password.length < 6 || req.body.password.length > 20) {
        res.status(400).json({
          message:
            "password must be at least 6 characters or at most 20 characters",
        })
        return
      }
      if (req.body.password != req.body.confirmpassword) {
        res.status(400).json({ message: "password not match" })
        return
      }
      if (req.body.password == req.body.username) {
        res
          .status(400)
          .json({ message: "Password must be different from username" })
        return
      }
      let re = /[0-9]/
      if (!re.test(password)) {
        res.status(400).json({
          message: "Password must contain at least one number (0 to 9)",
        })
        return
      }
      re = /[a-z]/
      if (!re.test(password)) {
        res.status(400).json({
          message: "Password must contain at least one lowercase letter (a-z)",
        })
        return
      }
      re = /[A-Z]/
      if (!re.test(password)) {
        res.status(400).json({
          message: "Password must contain at least one uppercase letter (A-Z)",
        })
        return
      }
    }
    try {
      //find name
      let result
      if (!email) {
        result = await client.query(
          "select * from member where username = $1",
          [username],
        )
      } else {
        result = await client.query(
          "select * from member where username = $1 or email = $2",
          [username, email],
        )
      }
      if (result.rowCount != 0) {
        res.status(409).json({ message: "User or email already exists" })
        return
      }
    } catch (error) {
      console.error("Failed to check user name duplication:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }
    let register: Register = {
      email,
      username,
      password,
      confirmpassword,
      is_admin,
    }
    console.log("insert register", register)

    let new_password = await hashPassword(req.body.password)
    try {
      await client.query(
        "INSERT INTO member (username,password,email,is_admin) VALUES ($1,$2,$3,$4)",
        [username, new_password, email, is_admin],
      )
    } catch (error) {
      console.error("Failed to insert member:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }

    res.json({ message: "Created member" })
  })

  app.get("/user", (req, res) => {
    // console.log(new Date().toTimeString(), 'get user', req.session.user)
    res.json({ user: req.session.user })
  })
  app.get("/member", adminOnly, async (req, res) => {
    try {
      let result = await client.query("select * from member order by id")
      res.json(result.rows)
    } catch (error: any) {
      res.status(500).json({ error: error.toString() })
    }
  })

  /*** 禁止刪除自己***/
  app.delete("/user/:id", adminOnly, async (req, res) => {
    // let user_id = req.session.user!.id
    let id = req.params.id
    // if (user_id == id){
    //   res.status(409).json({ message: "Can not delete your account" })
    //   return
    // }
    try {
      await client.query("delete from favorite_vtuber where user_id = $1", [id])
      await client.query("delete from post where user_id = $1", [id])
      await client.query("delete from favorite_video where user_id = $1", [id])
      await client.query("delete from user_log where user_id = $1", [id])
      await client.query("delete from member where id = $1", [id])

      res.json({ ok: true })
    } catch (error: any) {
      res.status(500).json({ error: error.toString() })
    }
  })

  app.post("/user/update", async (req, res) => {
    let id = req.body.user_id
    let { username, password, email, is_admin, exp } = req.body
    // let num = +exp
    // if (!num || null) {
    //   res.status(400).json({ message: "exp not is number" })
    //   return
    // }
    let fields = ["username"]
    for (let field of fields) {
      if (!req.body[field]) {
        res.status(400).json({ message: "missing " + field })
        return
      }
    }
    try {
      // updata vtuber info
      await client.query(
        /* sql */ `
              update member set
               username = $1,
                email = $2,
                is_admin = $3,
                exp = $4
              where id = $5
            `,
        [username, email, is_admin, exp, id],
      )
      if (password) {
        let new_password = await hashPassword(password)
        await client.query(
          /*sql*/ `update member set password = $1 where id = $2`,
          [new_password, id],
        )
      }
    } catch (error) {
      console.error("Failed to update user:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }

    res.json({ message: "Update user" })
  })

  app.get("/member/:id", async (req, res) => {
    try {
      let id = req.params.id
      let result = await client.query(
        /*SQL*/ `
      select * from member where id = $1`,
        [id],
      )
      if (result.rowCount == 0) {
        res.status(404).json({ error: "user not found" })
      } else {
        res.json({ user: result.rows[0] })
      }
    } catch (error: any) {
      res.status(500).json({ error: error.toString() })
    }
  })

  app.get("/admin", adminOnly, (req, res) => {
    res.json("why are you asking?")
  })

  app.post("/user/profile", memberOnly, (req, res) => {
    //
  })

  /** 更新ICON **/
  app.post("/user/icon/:id", upload.single("icon_file"), async (req, res) => {
    function cleanup() {
      if (req.file) {
        fs.unlink(req.file.path, (error) => {
          if (error) {
            console.error("Failed to cleanup icon photo:", error)
          }
        })
      }
    }
    let id = req.params.id
    let images = req.file?.filename
    if (!images) {
      res.status(400).json({ message: "missing icon_file" })
      cleanup()
      return
    }
    try {
      await client.query(
        /* sql */ `
              update member set
              icon = $1
              where id = $2
            `,
        [images, id],
      )
    } catch (error) {
      console.error("Failed to update icon:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }

    res.json({ message: "Update icon" })
  })

  /*** 給經驗 ***/
  // app.post("/user/exp", memberOnly,async (req,res) => {
  //   let id = req.session.user?.id
  //   let exp = req.query.exp
  //   try {
  //     await client.query(
  //             /* sql */ `
  //             update member set
  //             exp = exp+$1
  //             where id = $2
  //           `,
  //       [
  //         exp,
  //         id
  //       ],
  //     )
  //   } catch (error) {
  //     console.error("Failed to update exp:", error)
  //     res.status(500).json({ message: "Database Error" })
  //     return
  //   }

  //   res.json({ message: "update exp" })

  // })

  app.get("/user/exp", memberOnly, async (req, res) => {
    let id = req.session.user?.id
    try {
      let result = await client.query(
        /* sql */ `
              select 
              exp
              from member
              where id = $1
            `,
        [id],
      )
      let userExp = +result.rows[0].exp
      result = await client.query(
        /* sql */ `
        select
          count(*) as views
        from user_log
        where user_id = $1
      `,
        [id],
      )
      let userLog = +result.rows[0].views
      let Exp = userExp + userLog
      res.json({exp:Exp})
    } catch (error) {
      console.error("Failed to update exp:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }
  })

  app.get("/imageSlider", async (req, res) => {
    try {
      let result = await client.query(/* sql */ `
            select *
            from imageSlider
          `)
      let imageSilder = result.rows
      res.json(imageSilder)
    } catch (error) {
      console.error("Failed to update exp:", error)
      res.status(500).json({ message: "Database Error" })
      return
    }
  })
}
