export type User = {
  id: number
  email: string
  username: string
  // password: string
  password: string
  is_admin: boolean
  icon: string
}

export type VTuber = {
  id: number
  // jp_name: string
  // cn_name: string | null
  // en_name: string | null
  images: string | null
  twitter: string | null
  youtube: string | null
  vtuber_group_id: number | null
  period: string | null
  introduction: string | null
  // tag : string[]
  bilibili: string | null
}
export type VTuberTagItem = VTuber & {
  tag: string[]
}

export type VTuberGroup = {
  id: number
  group_name: string
  group_image: string
}

export type Video = {
  id: number
  title: string
  image: string
  video_url : string
}

export type InsertRow<Row> = Omit<Row, "id">

export type Register = {
  email: string
  username: string
  password: string
  confirmpassword: string
  is_admin : boolean| null
}

