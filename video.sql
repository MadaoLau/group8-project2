create database project2;

create table member (
	id serial primary key,
	username varchar(255) unique,
	password varchar(255),
    email VARCHAR(255),
    icon VARCHAR(255) null,
	created_at timestamp default current_timestamp,
    is_admin boolean not null default false
);

create table video (
	id serial primary key,
	title varchar(255) not null,
	image varchar(255) null,
    url varchar(255) unique not null,
	created_at timestamp default current_timestamp,
	updated_at timestamp
);

create table tag (
    id serial primary key,
    tag_name varchar(255) unique
);

create table favorite_video (
    user_id integer not null references member(id),
    video_idd integer not null references video(id)
);


create type status AS ENUM('active','pending');

create table post (
	id serial primary key,
	user_id integer not null references member(id),
	content text not null,
    status_current status default 'active',
    reply_id integer null references post(id)
);



create table post_like (
    post_id integer not null references post(id),
    user_id integer not null references member(id)
);

create table video_post (
    video_id integer not null references video(id),
    post_id integer not null references post(id)
);

create table post_tag (
    post_id integer not null references post(id),
    tag_id integer null references tag(id)
);

/*vtuber*/
create table lang (
    id serial primary key,
    lang varchar(10) unique
);

create table vtuber_group (
    id  serial primary key,
    group_name VARCHAR(30)  unique,
    group_image VARCHAR(255)
);

create table vtuber (
    id serial primary key,
    vtuber_group_id integer null references vtuber_group(id),
    introduction  VARCHAR(255) null,
    youtube VARCHAR(255) null,
    twitter VARCHAR(255) null,
    image  VARCHAR(255) null,
    period VARCHAR(30) null
);

-- ALTER TABLE vtuber RENAME COLUMN youtuber TO youtube;
ALTER TABLE vtuber
ADD bilibili VARCHAR(255);


create table vtuber_name (
    name VARCHAR(255) not null,
    vtuber_id  integer not null references vtuber(id),
    lang_id integer not null references lang(id)
);

create table favorite_vtuber (
    user_id integer not null references member(id),
    vtuber_id  integer not null references vtuber(id)
);

create table vtuber_tag (
    vtuber_id  integer not null references vtuber(id),
    tag_id integer null references tag(id)
);

/*26/12 ADD*/
create table category (
    id serial primary key,
    category VARCHAR(30) not null
);

ALTER TABLE video
ADD category_id integer null references category(id);

ALTER TABLE vtuber
ADD language VARCHAR(30) null;
ALTER TABLE vtuber
ADD url_3D VARCHAR(255) null;
ALTER TABLE vtuber
ADD recommend integer null;
ALTER TABLE video
ADD recommend integer null;
-- ALTER TABLE vtuber
-- ADD count integer null;
-- ALTER TABLE video
-- ADD count integer null;

DROP table post_tag;

create table video_tag (
    video_id integer not null references video(id),
    tag_id integer null references tag(id)
);

ALTER TABLE video RENAME COLUMN url TO video_url;


/* 28/12*/

ALTER TABLE member
ADD exp integer default 0;


create table imageSlider (
    id serial primary key,
    images VARCHAR(255) not null,
    title VARCHAR(255),
    imageLinks VARCHAR(255)
);


insert into imageSlider(images,title,imageLinks)  VALUES 
('https://assets.codepen.io/6124938/Konachan.com+-+267367.jpg',111,'https://assets.codepen.io/6124938/Konachan.com+-+267367.jpg')
, ('https://assets.codepen.io/6124938/Konachan.com+-+307514.jpg',111,'https://assets.codepen.io/6124938/Konachan.com+-+307514.jpg')
,('https://assets.codepen.io/6124938/Konachan.com+-+66171.jpg',111,'https://assets.codepen.io/6124938/Konachan.com+-+66171.jpg')
,('https://assets.codepen.io/6124938/Konachan.com+-+294174.jpg',111,'https://assets.codepen.io/6124938/Konachan.com+-+294174.jpg')
,('https://assets.codepen.io/6124938/Konachan.com+-+201232.jpg',111,'https://assets.codepen.io/6124938/Konachan.com+-+201232.jpg');


/*30*/

ALTER TABLE favorite_video RENAME COLUMN video_idd TO video_id;


/*Add*/

insert into lang (lang)  VALUES ('jp'),('cn'),('en');
-- insert into vtuber_group (group_name)  VALUES ('hololive'),('NIJISANJI'),('個人勢');
insert into category (category)  VALUES ('動畫'),('虛擬主播'),('音樂'),('遊戲');

-- alter table vtuber_name
-- drop constraint vtuber_name_lang_id_fkey;

-- ALTER TABLE vtuber_name
-- ADD CONSTRAINT vtuber_name_lang_id_fkey

-- ALTER TABLE vtuber_group
-- ADD group_image VARCHAR(255)

-- FOREIGN KEY (lang_id) REFERENCES lang (id);

-- delete from vtuber_name;
-- delete from vtuber_tag;
-- delete from vtuber;

-- alter table vtuber_name
-- drop constraint vtuber_name_name_key
-- ;


alter table video drop column count;

create table music(
    id serial primary key
    , music_name varchar(255) not null
    , music_photo VARCHAR(255)
    , music_file VARCHAR(255)
    , music_content text
    , created_at timestamp default current_timestamp
    , updated_at timestamp default current_timestamp
);

create table singer(
     id serial primary key
     , singer_name varchar(255)
);

alter table music add column singer_id integer;

alter table music
add foreign key (singer_id) references singer(id);

alter table music 
add column member_id integer
, add foreign key (member_id) references member(id);

alter table music
add column music_type varchar(20) not null;


create table music_like (
    music_id integer not null references music(id),
    user_id integer not null references member(id)
);


create table music_post (
	id serial primary key,
	user_id integer not null references member(id),
	content text not null,
    status_current status default 'active',
    reply_id integer null references music_post(id)
);

alter table music_post add column updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;


create table singer_post (
	id serial primary key,
	user_id integer not null references member(id),
	content text not null,
    status_current status default 'active',
    reply_id integer null references singer_post(id)
);

alter table singer_post add column updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;


create table user_log (
    user_id integer not null references member(id),
    timestamp timestamp default current_timestamp,
    video_id integer null references video(id),
    vtuber_id  integer null references vtuber(id),
    music_id  integer null references music(id)
);

alter table user_log
alter COLUMN user_id drop not null;

alter table user_log add column singer_id integer null references singer(id);