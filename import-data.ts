import { client } from "./db"

let list = require("./public/data/vtuber.json")
// console.log(list)

async function main() {
  let result = await client.query(
    /* sql */ `select id from lang where lang = 'jp'`,
  )
  let jp_lang_id = result.rows[0].id

  result = await client.query(/* sql */ `select id from lang where lang = 'cn'`)
  let cn_lang_id = result.rows[0].id

  for (let vtuber of list) {
    console.log("insert", vtuber.name)

    // prepare group
    // console.log("select vtuber group")
    result = await client.query(
      /* sql */ `
          select id from vtuber_group where group_name = $1
        `,
      [vtuber.group],
    )
    if (result.rowCount == 0) {
    //   console.log("insert vtuber group")
      result = await client.query(
        /* sql */ `
          insert into vtuber_group (group_name) values ($1) returning id
          `,
        [vtuber.group],
      )
    }
    let group_id = result.rows[0].id

    // insert vtuber
    // console.log("insert vtuber")
    result = await client.query(
      /* sql */ `
        insert into vtuber
        (image, youtube, bilibili, vtuber_group_id)
        values ($1, $2, $3, $4)
        returning id
      `,
      [vtuber.img, vtuber.Youtube, vtuber.Bilibili, group_id],
    )
    let vtuber_id = result.rows[0].id

    // insert vtuber name
    // console.log("insert vtuber jp name")
    await client.query(
      /* sql */ `
          insert into vtuber_name
          (vtuber_id, lang_id, name)
          values ($1, $2, $3)
        `,
      [vtuber_id, jp_lang_id, vtuber.jpname],
    )
    // console.log("insert vtuber cn name")
    await client.query(
      /* sql */ `
            insert into vtuber_name
            (vtuber_id, lang_id, name)
            values ($1, $2, $3)
          `,
      [vtuber_id, cn_lang_id, vtuber.name],
    )
  }

  await client.end()
}
main()
