
import { config } from 'dotenv'
import { Application } from 'express'
import session from 'express-session'



declare module 'express-session' {
	interface SessionData {
		views: number
		user:{
			id: number
			username:string
			login: boolean
			is_admin : boolean
			icon : string
		}
	}
}
export function attachSession(app: Application) {
	config()
    if (!process.env.SESSION_SECRET) {
        throw new Error('missing SESSION_SECRET in env')
    }

app.use(session({
	secret: process.env.SESSION_SECRET,
	resave: true,
	saveUninitialized: true,
}))
}