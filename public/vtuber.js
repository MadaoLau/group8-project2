let groupList = document.querySelector(".vtuber-row")
let groupItemTemplate = groupList.querySelector(".vtuber-col")
let figureListTemplate = groupList.querySelector(".group-detail-display")
let figureItemTemplate =
  figureListTemplate.querySelector(".vtuber-display")

groupItemTemplate.remove()
figureListTemplate.remove()
figureItemTemplate.remove()

let groupPerLine = 1

let groups = {}
let group_images = {}
async function loadGroups() {
  // let res = await fetch("data/vtuber.json")
  let res = await fetch("/vtuber_group")
  let figures = await res.json()
  groups = {}
  for (let figure of figures) {
    if (figure.group in groups) {
      groups[figure.group].push(figure)
    } else {
      groups[figure.group] = [figure]
      group_images[figure.group] = figure.group_image
    }
  }
  // console.log(groups)
  let i = 0
  for (let groupName in groups) {
    let group = groups[groupName]
    let groupItem = groupItemTemplate.cloneNode(true)
    let groupIndex = i
      // let display = $(".group-detail-display")
    let group_image = group_images[groupName]
    if (group_image) {
      groupItem.querySelector(".group-icon").src = group_image
    }
    groupItem.querySelector(".group-name-span").textContent = groupName
    groupItem.addEventListener("click", () => {
      let figureListIndex = Math.floor(groupIndex / groupPerLine)
      let figureList = groupList.querySelectorAll(
        ".group-detail-display",
      )[figureListIndex]
      let display = $(figureList)

      // // figureList.classList.add("active")
      // showGroup(figureList, groupItem, group)

      // // display.slideUp()
      // display.slideDown()

      // return

      groupList
        .querySelectorAll(".group-detail-display.active")
        .forEach((e) => e.classList.remove("active"))
        // display.slideDown()
      console.log("group item", groupItem)
      console.log("figure list", figureList)

      if (groupItem.classList.contains("active")) {
        groupItem.classList.remove("active")
        console.log('1 active slide up')
        display.slideUp("slow",function(){console.log("success run")})
      } else {
        groupList
          .querySelectorAll(".vtuber-col.active")
          .forEach((e) => e.classList.remove("active"))
        groupItem.classList.add("active")
        let figureListIndex = Math.floor(groupIndex / groupPerLine)
        let figureList = groupList.querySelectorAll(
          ".group-detail-display",
        )[figureListIndex]
        figureList.classList.add("active")
        console.log('2 inactive slide up')
        display.slideUp("slow",function(){console.log("success 2 run")})
        
        setTimeout(() => {
          showGroup(figureList, groupItem, group)
          
          display.slideDown("slow",function(){console.log('3 inactive slide down')})
        }, 400)
      }
    })
    groupList.appendChild(groupItem)
    i++
  }
  resizeGroups()
}
loadGroups()

function resizeGroups() {
  groupList
    .querySelectorAll(".group-detail-display")
    .forEach((e) => e.remove())
  let groupItemWidth = groupList
    .querySelector(".vtuber-col")
    .getBoundingClientRect().width
  groupList.appendChild(figureListTemplate)
  let figureListWidth = figureListTemplate.getBoundingClientRect().width
  figureListTemplate.remove()
  groupPerLine = Math.floor(figureListWidth / groupItemWidth)
  let groupItemList = groupList.querySelectorAll(".vtuber-col")
  console.log({
      groupItemWidth,
      figureListWidth,
      groupPerLine,
    })
    // return
  for (
    let i = groupPerLine; i < groupItemList.length; i += groupPerLine
  ) {
    let figureList = figureListTemplate.cloneNode(true)
    figureList.classList.remove("active")
    console.log("insert before", {
      i,
      n: groupItemList.length,
    })
    groupList.insertBefore(figureList, groupItemList[i])
  }
  let figureList = figureListTemplate.cloneNode(true)
  figureList.classList.remove("active")
  groupList.appendChild(figureList)
}

window.addEventListener("resize", resizeGroups)

function showGroup(figureList, groupItem, group) {
  figureList.textContent = ""
  for (let figure of group) {
    let figureItem = figureItemTemplate.cloneNode(true)
    figureItem.querySelector(".vtuber-link").href += "?id=" + figure.id
    figureItem.querySelector("img").src = figure.img
    figureItem.querySelector(".vtuber-name-span").textContent =
      figure.name
    figureList.appendChild(figureItem)
  }
  console.log("showGroup", {
    figureList,
    groupItem,
    group,
  })
}



let addVTuberForm = document.querySelector("#add-vtuber-form")
let addVTuberImg = addVTuberForm.querySelector(".vtuber-imges")
let companyList = document.querySelector("#companyList")

addVTuberForm.pic_link.addEventListener("change", () => {
  addVTuberImg.src = addVTuberForm.pic_link.value
  addVTuberForm.pic_file.value = ""
})
addVTuberForm.pic_file.addEventListener("change", () => {
  let file = addVTuberForm.pic_file.files[0]
  if (!file) return
  addVTuberForm.pic_link.value = ""
  readFile(file, (dataUrl) => {
    addVTuberImg.src = dataUrl
  })
})

// let addGroupForm = document.querySelector("#add-group-form")
// let addGroupImg = addGroupForm.querySelector(".group-imges")
// addGroupForm.gp_link.addEventListener("change", () => {
//   addGroupImg.src = addGroupForm.gp_link.value
//   addGroupForm.gp_file.value = ""
// })
// addGroupForm.gp_file.addEventListener("change", () => {
//   let file = addGroupForm.gp_file.files[0]
//   if (!file) return
//   addGroupForm.gp_link.value = ""
//   readFile(file, (dataUrl) => {
//     addGroupImg.src = dataUrl
//   })
// })

function readFile(file, cb) {
  let reader = new FileReader()
  reader.onload = () => {
    cb(reader.result)
  }
  reader.readAsDataURL(file)
}

let upload = document.querySelector(".uploadphoto")
let url = document.querySelector(".urlphoto")
upload.addEventListener("click", photoList)
url.addEventListener("click", photoList2)

function photoList() {
  upload.classList.add("active")
  url.classList.remove("active")
  document.querySelector(".upload-file").classList.add("active")
  document.querySelector(".upload-url").classList.remove("active")
}

function photoList2() {
  upload.classList.remove("active")
  url.classList.add("active")
  document.querySelector(".upload-file").classList.remove("active")
  document.querySelector(".upload-url").classList.add("active")
}

// let upload_group = document.querySelector(".uploadGroupPhoto")
// let url_group = document.querySelector(".urlGroupPhoto")
// upload_group.addEventListener("click", groupList)
// url_group.addEventListener("click", groupList)

// function groupList() {
//   upload_group.classList.toggle("active")
//   url_group.classList.toggle("active")
//   document.querySelector(".upload-group-file").classList.toggle("active")
//   url_link = document
//     .querySelector(".upload-group-url")
//     .classList.toggle("active")
// }

async function initVTuber() {
  let res = await fetch("/company")
  let companyRows = await res.json()
  for (let company of companyRows) {
    let option = document.createElement("option")
    option.value = company.group_name
    companyList.appendChild(option)
  }

  addVTuberForm.addEventListener("submit", async(event) => {
    event.preventDefault()
    let title = "createVtuber"
    let form = addVTuberForm

    let group_name = form.querySelector("#company").value
    let company = companyRows.find((row) => row.group_name == group_name)
    if (!company) {
      showFormResult({
        form,
        title,
        message: "VTuber Group Not Found",
        isSuccess: false,
      })
      return
    }
    form.vtuber_group_id.value = company.id
    let body = new FormData(form)
    try {
      let res = await fetch(form.action, {
          method: form.method,
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(body),
        })
        // console.log('create memo res:', res)
      let json = await res.json()
        // console.log('create memo json:', json)
      if (200 <= res.status && res.status < 300) {
        showFormResult({ form, title, message: json.message, isSuccess: true })
      } else {
        showFormResult({ form, title, message: json.message, isSuccess: false })
      }
    } catch (error) {
      showFormResult({
        form,
        title,
        message: error.toString(),
        isSuccess: false,
      })
    }
  })
}
initVTuber()