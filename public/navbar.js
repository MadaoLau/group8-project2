let navbar = document.querySelector('nav')

navbar.innerHTML = /*html */ `
<div class="menu-2-bg"></div>
<header class="header">
<div class="header-container">
    <div class="header-left">
        <img src="images/logo.png" alt="logo" class="layout-logo-img">
    </div>
    <div class="header-center">
        <nav class="header-nav">
            <a class="header-link" href="index.html">Home</a>
            <a class="header-link" href="vtuber.html">Vtuber</a>
            <a class="header-link" href="video.html">Video</a>
            <a class="header-link" href="cinema.html">Arts</a>
            <a class="header-link" href="music.html">Music</a>
        </nav>
    </div>
    <div class="header-right">
        <div class="select-box">
            <div class="select-box-input">
                <input placeholder="搜索..." class="search-input"></div>
            <div class="seach-icon"><i class="fas fa-search"></i></div>
        </div>
        <div class="user-control-wrap login" hidden>
            <div class="language-ball">
                <i class="fas fa-globe-americas"></i>
            </div>
            <a href="/login.html"><i class="fas fa-user profit user-unlogged-wrap"></i></a>
            <span class="login"><a href="/login.html">登錄 | 註冊</a></span>
        </div>
        <div class="user-control-wrap logout" hidden>
            <div class="user-logged-wrap">
                <div class="user-icon">
                    <img class="usericon" src="/images/default.png">
                </div>
                <form class="profile_dd" method="post" action="/logout">
                        <div><a class="username">Amen</a></div>
                        <div class="experience">
                        <div id="scoreBar" class="bar">&nbsp;
                        </div>
                        <div id="scoreDisplay" class="points"></div>

                        <div class="level"></div>
                        </div>
                        <div class="profile_ul">
                        <div class="profile_li"><a class="profile" href="#"><span class="picon"><i class="fas fa-user-alt"></i>
                          </span>Profile</a>
                        </div>
                        <div><a class="settings" href="/user-password.html"><span class="picon"><i class="fas fa-cog"></i></span>Settings</a></div>
                        <div><a onclick="logout()" class="logout" href="#"><span class="picon"><i class="fas fa-sign-out-alt"></i></span>Logout</a></div>
                        <div><a class="adminpage" href="/admin.html" hidden><span class="picon"><i class="fas fa-users-cog"></i></span>Admin</a></div>
                    </div>
                </form>
                <div class="icon-wrap">
                    <div class="ivu-poptip">
                        <div class="ivu-badge">
                            <i class="far fa-envelope"></i>
                        </div>
                        <div class="notification_dd">
                            <ul class="notification_ul">
                                <li class="starbucks success">
                                    <div class="notify_icon">
                                        <span class="icon"></span>
                                    </div>
                                    <div class="notify_data">
                                        <div class="title">
                                            Hololive
                                        </div>
                                        <div class="sub_title">
                                            new role La+ Darknesss
                                        </div>
                                    </div>
                                    <div class="notify_status">
                                        <p>Success</p>
                                    </div>
                                </li>
                                <li class="show_all">
                                    <p class="link">Show All Activities</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="ivu-badge">
                    <div class="icon-wrap">
                        <i class="far fa-clock"></i>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</header>
<div class="popup">
<div class="shadow"></div>
<div class="inner_popup">
    <div class="notification_dd">
        <ul class="notification_ul">
            <li class="title">
                <p>All Notifications</p>
                <p class="close"><i class="fas fa-times" aria-hidden="true"></i></p>
            </li>
            <li class="starbucks success">
                <div class="notify_icon">
                    <span class="icon"></span>
                </div>
                <div class="notify_data">
                    <div class="title">
                        Hololive
                    </div>
                    <div class="sub_title">
                        new role La+ Darknesss
                    </div>
                </div>
                <div class="notify_status">
                    <p>Success</p>
                </div>
            </li>
        </ul>
    </div>
</div>
</div>`



let loginLink = document.querySelector('.user-control-wrap.login')
let logoutForm = document.querySelector('.profile_dd')
let userBar = document.querySelector('.user-control-wrap.logout')


async function loadUser() {
  // console.log(new Date().toTimeString(), 'loadUser')
  let res = await fetch('/user')
    // console.log(new Date().toTimeString(), 'got res')
  let json = await res.json()
    // console.log(new Date().toTimeString(), 'got json')
  if (json.user) {
    loginLink.setAttribute('hidden', '')
    userBar.removeAttribute('hidden')
    document.querySelector('.username').textContent = json.user.username
    document.querySelector('.profile').href = "/user-space.html?id=" + json.user.id
    document.querySelector('.username').href = "/user-space.html?id=" + json.user.id

    document.querySelector('.fa-clock').addEventListener('click', () => {
      location.href = "/user-space.html?id=" + json.user.id
    })
    if (json.user.icon) {
      document.querySelector('.usericon').src = json.user.icon
    }
    if (json.user.is_admin) {
      document.querySelector('.adminpage').removeAttribute('hidden')
    }
  } else {
    userBar.setAttribute('hidden', '')
    loginLink.removeAttribute('hidden')
  }
}

loadUser()