let addVideoForm = document.querySelector("#add-video-form")
let addVideoImg = addVideoForm.querySelector(".video-imges")

var curPage = 1
var totalPages = 15
let catId = 0
let word = ""


addVideoForm.pic_link.addEventListener("change", () => {
  addVideoImg.src = addVideoForm.pic_link.value
  addVideoForm.pic_file.value = ""
})
addVideoForm.pic_file.addEventListener("change", () => {
  let file = addVideoForm.pic_file.files[0]
  if (!file) return
  addVideoForm.pic_link.value = ""
  readFile(file, (dataUrl) => {
    addVideoImg.src = dataUrl
  })
})

function readFile(file, cb) {
  let reader = new FileReader()
  reader.onload = () => {
    cb(reader.result)
  }
  reader.readAsDataURL(file)
}

let upload = document.querySelector(".uploadphoto")
let url = document.querySelector(".urlphoto")
upload.addEventListener("click", photoList)
url.addEventListener("click", photoList2)

function photoList() {
  upload.classList.add("active")
  url.classList.remove("active")
  document.querySelector(".upload-file").classList.add("active")
  document.querySelector(".upload-url").classList.remove("active")
}

function photoList2() {
  upload.classList.remove("active")
  url.classList.add("active")
  document.querySelector(".upload-file").classList.remove("active")
  document.querySelector(".upload-url").classList.add("active")
}

let videoList = document.querySelector("#videoList")

async function initVideo() {
  let res = await fetch("/category")
  let categoryRows = await res.json()
  for (let category of categoryRows) {
    let option = document.createElement("option")
    option.value = category.category
    videoList.appendChild(option)
  }

  addVideoForm.addEventListener("submit", async(event) => {
    event.preventDefault()
    let title = "createVideo"
    let form = addVideoForm

    let name = form.querySelector("#video").value
    let category = categoryRows.find((row) => row.category == name)
    if (!category) {
      showFormResult({
        form,
        title,
        message: "Category Not Found",
        isSuccess: false,
      })
      return
    }
    form.category_id.value = category.id
    let body = new FormData(form)
    try {
      let res = await fetch(form.action, {
        method: form.method,
        body,
      })
      let json = await res.json()
      if (200 <= res.status && res.status < 300) {
        showFormResult({ form, title, message: json.message, isSuccess: true })
        loadVideo()
      } else {
        showFormResult({ form, title, message: json.message, isSuccess: false })
      }
    } catch (error) {
      showFormResult({
        form,
        title,
        message: error.toString(),
        isSuccess: false,
      })
    }
  })
}
initVideo()

/*** 少一個 ***/
let category_list = document.querySelector(".copy_searchword")
let category_template = category_list.querySelector("p")
category_template.remove()

async function loadCategory() {
  let res = await fetch("/category")
  let categoryRows = await res.json()

  for (let category of categoryRows) {
    showCategory(category)
  }
}

loadCategory()

async function showCategory(category) {
  console.log("showCategory", category)
  let category_node = category_template.cloneNode(true)
  category_node.querySelector("a").textContent = category.category
  category_node.querySelector("a").addEventListener("click", () => {
    // loadCategoryVideo(category)
    catId = category.id
    word = ""
    loadVideo()
  })
  category_list.appendChild(category_node)
}

/*** 顯示唔到分類 ***/
async function loadCategoryVideo(category) {
  let res = await fetch("/category/" + category.id)
  let videoRows = await res.json()
  allplaylist.querySelectorAll(".swiper-slide").forEach((e) => e.remove())
  for (let video of videoRows) {
    showVideo(video)
  }
}

let allplaylist = document.querySelector(".allplaylist")
let swiperSlide = allplaylist.querySelector(".swiper-slide")
swiperSlide.remove()

async function loadVideo() {
  let params = new URLSearchParams()
  params.set('page', curPage)
  params.set('cat', catId)
  params.set('word', word)
  let res = await fetch("/video?" + params)
  let json = await res.json()
  let videoRows = json.videoList
  let totalPages = Math.ceil(json.videoTotal / 8)
  console.log(totalPages)
  generatePagination(curPage, totalPages, options)
  gonow()
  allplaylist.querySelectorAll(".swiper-slide").forEach((e) => e.remove())
  for (let video of videoRows) {
    showVideo(video)
  }
}

async function showVideo(video) {
  let video_node = swiperSlide.cloneNode(true)
  if (video.image) {
    video_node.querySelector(".video-img").src = video.image
  }
  if (video.icon) {
    video_node.querySelector(".user-icon").src = video.icon
  }
  let fields = ["username", "title", "count"]
  for (let field of fields) {
    video_node.querySelector("." + field).textContent = video[field]
  }
  video_node.querySelector(".videoUrl").href += "?id=" + video.id
  video_node.querySelector(".videoUrl2").href += "?id=" + video.id
  video_node.querySelector(".userUrl").href += "?id=" + video.user_id
  video_node.querySelector(".userUrl2").href += "?id=" + video.user_id
  allplaylist.appendChild(video_node)
}

loadVideo()

var options = {
  target: ".page",
  pagesBeforeAndAfter: 2,
  visibleFirstAndLast: true,
  listItemClass: "",
  disabledClass: "",
  stringClass: "",
}


generatePagination(curPage, totalPages, options)

function parsePaginationAttributes(attributes) {
  attributes = attributes || {}

  attributes.target = attributes.target || ".page"
  attributes.pagesBeforeAndAfter = attributes.pagesBeforeAndAfter || 1

  attributes.showInvisibleOffset = 2
  if (!attributes.visibleFirstAndLast) {
    attributes.visibleFirstAndLast = false
    attributes.showInvisibleOffset = 1
  }

  attributes.listItemClass = attributes.listItemClass || "pagination-item"
  attributes.disabledClass =
    attributes.disabledClass || "pagination-item-disabled"
  attributes.stringClass = attributes.stringClass || "pagination-string"

  return attributes
}

function generatePagination(page, pages, attributes) {
  attributes = parsePaginationAttributes(attributes)
  var $target = $(attributes.target)
  $target.empty()

  // show first item
  if (
    attributes.visibleFirstAndLast &&
    page - attributes.pagesBeforeAndAfter > attributes.showInvisibleOffset - 1
  ) {
    $target.append(
      '<li class="' +
      attributes.listItemClass +
      '" data-page="' +
      1 +
      '">' +
      1 +
      "</li>",
    )
  }

  // dots before
  if (page - attributes.pagesBeforeAndAfter > attributes.showInvisibleOffset) {
    $target.append(
      '<li class="' +
      attributes.listItemClass +
      " " +
      attributes.disabledClass +
      '">...</li>',
    )
  }

  // genetate elements
  var $elem
  for (var i = 1; i <= pages; i++) {
    if (
      page + attributes.pagesBeforeAndAfter >= i &&
      page - attributes.pagesBeforeAndAfter <= i
    ) {
      $elem = $(
        '<li class="' +
        attributes.listItemClass +
        '" data-page="' +
        i +
        '">' +
        i +
        "</li>",
      )
      if (i == page) {
        $elem.addClass("active")
      }
      $target.append($elem)
    }
  }

  // dots after
  if (
    page + attributes.pagesBeforeAndAfter <=
    pages - attributes.showInvisibleOffset
  ) {
    $target.append(
      '<li class="' +
      attributes.listItemClass +
      " " +
      attributes.disabledClass +
      '">...</li>',
    )
  }

  // show last item
  if (
    attributes.visibleFirstAndLast &&
    page + attributes.pagesBeforeAndAfter <=
    pages - (attributes.showInvisibleOffset - 1)
  ) {
    $target.append(
      '<li class="' +
      attributes.listItemClass +
      '" data-page="' +
      pages +
      '">' +
      pages +
      "</li>",
    )
  }

  // page to link to next page
  if (page < pages) {
    var nextPage = page + 1
    $target.append(
      '<li class="' +
      attributes.listItemClass +
      " " +
      attributes.stringClass +
      '" data-page="' +
      nextPage +
      '">Next</li>',
    )
  }
  // page to link to previous page
  if (page > 1) {
    var prevPage = page - 1
    $target.append(
      '<li class="' +
      attributes.listItemClass +
      " " +
      attributes.stringClass +
      '"' +
      prevPage +
      '">Prev</li>',
    )
  }
}

function gonow() {
  let pagego = document.querySelectorAll('.pagination-item')
  for (let go of pagego) {

    go.addEventListener('click', () => {
      if (+go.textContent) {
        curPage = go.dataset.page
      }
      if (go.textContent == 'Next') {
        curPage++
      }
      if (go.textContent == "Prev") {
        curPage--
      }
      loadVideo()
    })
  }
}


let search = document.querySelector("form.search-video")
search.addEventListener("submit", async(event) => {
  event.preventDefault()
  word = search.key.value
  console.log(search.key.value)
  loadVideo()

})