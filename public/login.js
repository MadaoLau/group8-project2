function showRegister() {
  document.querySelector(".sign").className = "sign show-registers"
}

function showLogin() {
  document.querySelector(".sign").className = "sign show-login"
}

let loginForm = document.querySelector("form.login")
loginForm.addEventListener("submit", async (event) => {
  event.preventDefault()
  let form = loginForm
  let title = "login"
  let body = {
    username: form.username.value,
    password: form.password.value,
  }
  try {
    let res = await fetch("/login", {
      method: form.method,
      headers: {
        "Content-Type": " application/json",
      },
      body: JSON.stringify(body),
    })
    let json = await res.json()
    if (200 <= res.status && res.status < 300) {
      Swal.fire({
        title: "login",
        text: json.message,
        icon: "success",
      }).then((result) => {
        history.back()
        return
      })
      setTimeout(() => history.back(), 3000)
    } else {
      showFormResult({
        form,
        title,
        message: json.message,
        isSuccess: false,
      })
    }
  } catch (error) {
    showFormResult({
      form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
})

let registers = document.querySelector("form.register")
registers.addEventListener("submit", async (event) => {
  event.preventDefault()
  let form = registers
  let title = "register"
  let body = {
    email: form.email.value,
    username: form.username.value,
    password: form.password.value,
    confirmpassword: form.confirmpassword.value,
  }
  try {
    let res = await fetch("/register", {
      method: form.method,
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    })
    let json = await res.json()
    if (200 <= res.status && res.status < 300) {
      // showFormResult({
      //         form,
      //         title,
      //         message: json.message,
      //         isSuccess: true
      //     })
      Swal.fire({
        title: "register",
        text: json.message,
        icon: "success",
      }).then((result) => {
        history.back()
        return
      })
      setTimeout(() => history.back(), 3000)
    } else {
      showFormResult({
        form,
        title,
        message: json.message,
        isSuccess: false,
      })
    }
  } catch (error) {
    showFormResult({
      form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
})