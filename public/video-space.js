async function loadVideoSpace() {
  let params = new URLSearchParams(location.search)
  let id = params.get("id")
  let res = await fetch("/video/" + id)
  let json = await res.json()
  if (json.error) {
    location.href = '/'
    return
  }
  let video = json.video
  showVideo(video)
  console.log(video.id)
}
loadVideoSpace()

let header = document.querySelector(".header-left")
let hearderImg = header.querySelector("img").remove()

function showback() {
  let div = document.createElement("div")
  div.className = "layout-back"
  div.innerHTML = /* html */ `
    <div class="button-pc-animate" onclick="history.back()">
    <i class="fas fa-chevron-left"></i></div>`
  header.appendChild(div)
}

showback()


let videoTop = document.querySelector(".main-video")
let videoInfo = document.querySelector(".card-info")
let key = document.querySelector(".key")
let contents = document.querySelector(".contents")

function showVideo(video) {
  console.log("showVideo", video)
    // onYouTubeIframeAPIReady(video)

  let fields = ["title", "count", "username", "content"]
  for (let field of fields) {
    console.log("display", field)
    videoTop.querySelector("." + field).textContent = video[field]
  }
  if (video.icon) {
    document.querySelector(".icon").src = video.icon
  }
  document.querySelector('.video_user_icon').href += "?id=" + video.user_id
  for (keys of video.keyword) {
    let key_node = key.cloneNode(true)
    keys = ('#' + keys)
    key_node.textContent = keys
    contents.appendChild(key_node)
  }

  //   video_url
  let date = new Date(video.created_at)
  document.querySelector(".year").textContent = date.getFullYear()
  document.querySelector(".month").textContent = date.getMonth() + 1
  document.querySelector(".date .date").textContent = date.getDate()

  let player = new YT.Player("muteYouTubeVideoPlayer", {
    videoId: video.video_url, // YouTube 影片 ID
    width: 560, // 播放器寬度 (in px)
    height: 315, // 播放器長度 (in px)
    playerVars: {
      autoplay: 1, // 自動播放視頻
      controls: 1, // 顯示播放/暫停按鈕
      showinfo: 0, // 隱藏影片標題
      modestbranding: 1, // 隱藏YouTube LOGO
      loop: 0, // 循環播放
      playlist: video.video_url,
      fs: 1, // 隱藏全螢幕視窗按鈕
      cc_load_policty: 0, // 隱藏關閉字幕
      iv_load_policy: 3, // 隱藏影片註釋
      autohide: 0, // 播放時隱藏影片控制按鈕
    },
  })

}

// function onYouTubeIframeAPIReady(video) {
//   console.log("onYouTubeIframeAPIReady")
// }

let messageBox = document.querySelector('.message-box')

async function loadVideoUser() {
  let res = await fetch('/user')
  let json = await res.json()
  if (json.user) {
    if (json.user.icon) {
      messageBox.querySelector('.usericon').src = json.user.icon
    }
    // if (json.user.is_admin) {
    //   document.querySelector('.adminpage').removeAttribute('hidden')
    // }
  } else {
    messageBox.setAttribute('hidden', '')
  }
}

loadVideoUser()



let messageForm = document.querySelector("form.input-message")
messageForm.addEventListener("submit", async(event) => {
  event.preventDefault()
  let params = new URLSearchParams(location.search)
  let id = params.get("id")
  let form = messageForm
  let title = "message"
  let body = {
    content: form.content.value,
  }
  try {
    let res = await fetch("/video/message/" + id, {
      method: form.method,
      headers: {
        "Content-Type": " application/json",
      },
      body: JSON.stringify(body),
    })
    let json = await res.json()
    if (200 <= res.status && res.status < 300) {
      showFormResult({ form, title, message: json.message, isSuccess: true })
    } else {
      showFormResult({
        form,
        title,
        message: json.message,
        isSuccess: false,
      })
    }
  } catch (error) {
    showFormResult({
      form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
})


let messageOutput = document.querySelector('.message-output')
let Output = messageOutput.querySelector('.output-box')
Output.remove()


async function loadContent() {
  let params = new URLSearchParams(location.search)
  let id = params.get("id")
  let res = await fetch(`/video/${id}/content`)
  let json = await res.json()
  for (let content of json) {
    console.log(content)
    showContent(content)
  }
}

async function showContent(content) {
  let messageItem = Output.cloneNode(true)
  messageItem.querySelector(".user_message_icon").href += "?id=" + content.id
  messageItem.querySelector(".user_name").textContent = content.username
  messageItem.querySelector(".user_name").href += "?id=" + content.id
  if (content.image) {
    messageItem.querySelector("img").src = content.image
  }
  messageItem.querySelector(".output-content").textContent = content.content
  messageItem.querySelector(".adminContent").addEventListener('click', () => {
    deleteContent(content, messageItem)
  })

  messageOutput.appendChild(messageItem)
}

loadContent()


async function deleteContent(content, messageItem) {
  console.log("del", content.post)
    // TODO ask user to confirm
  let result = await Swal.fire({
      title: "Confirm to delete " + content.content + " ?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
    },

    "",
    "question",
  )
  console.log(result)
  if (!result.isConfirmed) {
    return
  }
  let res = await fetch("/post/" + content.post, { method: "DELETE" })
  let json = await res.json()
  if (json.error) {
    // TODO show error
    Swal.fire("Failed to delete message", json.error, "error")
    return
  }
  Swal.fire("Deleted " + content.content, json.error, "error")
  messageItem.remove()
}


async function loadNewVideo() {
  let res = await fetch("/video")
  let videoRows = await res.json()
  videoList = videoRows.videoList.slice(0, 8)
  for (let video of videoList) {
    console.log(video)
    showNewVideo(video)
  }
}


let newVideoDisplay = document.querySelector('.video-list')
let videoDisplay = newVideoDisplay.querySelector('.vid2')
videoDisplay.remove()

async function showNewVideo(video) {
  let VideoItem = videoDisplay.cloneNode(true)
  VideoItem.querySelector("a").href += "?id=" + video.id

  VideoItem.querySelector(".title2").textContent = video.title

  if (video.image) {
    VideoItem.querySelector("img").src = video.image
  }
  newVideoDisplay.appendChild(VideoItem)
}

loadNewVideo()



let params = new URLSearchParams(location.search)
let id = params.get("id")

let likeButton = document.querySelector(".video-like")
likeButton.addEventListener("click", async() => {
  let title = "like"
  try {
    let res = await fetch(`/video/${id}/like`, {
      method: hasLiked ? "DELETE" : "POST",
    })
    let json = await res.json()
    console.log('json:', json);

    let likeNumber = json.likes
    if (json.error) {
      showFormResult({ title, message: json.message, isSuccess: false })
    } else {
      console.log(likeNumber)
        // likecount.textContent = json.likes
      hasLiked = !hasLiked
      if (hasLiked) {
        unlike.setAttribute("hidden", "")
        like.removeAttribute("hidden")
      } else {
        unlike.removeAttribute("hidden")
        like.setAttribute("hidden", "")
      }
    }
    checkLike()
  } catch (error) {
    showFormResult({
      // form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
})

/** like 未做到 */
let unlike = document.querySelector(".unlike")
let like = document.querySelector(".like")
let hasLiked
let title = 'like'
let likecount = document.querySelector('.likecount')
async function checkLike() {
  let res = await fetch("/user")
  let json = await res.json()
  if (json.user.is_admin) {
    document.querySelector('.trash').removeAttribute("hidden")
    let adminContent = document.querySelectorAll('.adminContent')
    for (let content of adminContent) {
      content.removeAttribute("hidden")
    }
  }
  try {
    let res = await fetch(`/video/${id}/like`)
    json = await res.json()
    hasLiked = json.hasLiked
    likecount.textContent = json.likes
    if (json.error) {
      // showFormResult({ title, message: json.message, isSuccess: false })
    } else if (hasLiked) {
      console.log("hasLiked", hasLiked);
      unlike.setAttribute("hidden", "")
      like.removeAttribute("hidden")
    } else {
      unlike.removeAttribute("hidden")
      like.setAttribute("hidden", "")
    }
  } catch (error) {
    showFormResult({
      // form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
}
checkLike()



let deleteButton = document.querySelector(".trash")
deleteButton.addEventListener("click", async() => {
  let title = "delete"
  try {
    let res = await fetch(`/video/${id}`, {
      method: "DELETE",
    })
    let json = await res.json()
    console.log('json:', json);

    let likeNumber = json.likes
    if (json.error) {
      showFormResult({ title, message: json.message, isSuccess: false })
    } else {
      showFormResult({ title, message: json.message, isSuccess: true })
    }
  } catch (error) {
    showFormResult({
      // form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
})