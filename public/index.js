//header nav
window.onscroll = () => {

  if (window.scrollY > 100) {
    document.querySelector('header').classList.add('header-active');
  } else {
    document.querySelector('header').classList.remove('header-active');
  }

}

function showFormResult({ form, title, message, isSuccess }) {
  let icon
  if (isSuccess) {
    form.reset()
    icon = 'success'
  } else {
    icon = 'error'
  }
  Swal.fire({
    title: title,
    text: message,
    icon: icon,
  })
}


// let user_login = document.querySelector('.user-control-wrap')
// user_login.addEventListener('click', () => {
//   location.href = "/login.html"
// })


$(".user-logged-wrap .user-icon").click(function() {
  $(this).parent().toggleClass("active");
  $(".ivu-poptip").removeClass("active");
});

$(".ivu-poptip .ivu-badge").click(function() {
  $(this).parent().toggleClass("active");
  $(".user-logged-wrap").removeClass("active");
});

$(".show_all .link").click(function() {
  $(".ivu-poptip").removeClass("active");
  $(".popup").show();
});

$(".close, .shadow").click(function() {
  $(".popup").hide();
});

window.addEventListener('pageshow', (event) => {
  var historyTraversal = event.persisted ||
    (typeof window.performance != "undefined" &&
      window.performance.navigation.type === 2);
  if (historyTraversal) {
    // Handle page restore.
    window.location.reload();
  }
})

async function logout() {
  let form = logoutForm
  let title = 'logout'
  try {
    let res = await fetch(form.action, {
      method: form.method,
    })
    let json = await res.json()
    if (200 <= res.status && res.status < 300) {
      showFormResult({ form, title, message: json.message, isSuccess: true })
      await loadUser()
        // Swal.fire({
        //     title: 'Logout',
        //     text: json.message,
        //     icon: 'success',
        // })
    } else {
      showFormResult({ form, title, message: json.message, isSuccess: false })

    }
  } catch (error) {
    showFormResult({ form, title, message: error.toString(), isSuccess: false })
  }
}

logoutForm.addEventListener('submit', async event => {
  event.preventDefault()
  logout()
})


let IknowLevel = [-1, 100, 500, 1000, 2500, 5000, 8000, 12000, 16000, 20000, 25000, 35000, 50000, 80000, 120000, 180000, 250000, 400000];

function scoreRake(score) {
  var len = IknowLevel.length;
  var i = getUserLevel(score);
  var min, max, rake;
  if (i == 0) return 0;
  if (i >= len) {
    return 100;
  }
  min = IknowLevel[i - 1];
  max = IknowLevel[i];
  if (score > min && score <= max)
    return (score - min) * 100 / (max - min);
  return 0;
}

function getUserLevel(score) {
  for (i = 0, l = IknowLevel.length; i < l; i++) {
    if (score <= IknowLevel[i]) break;
  }
  return i;
}

function scoreBar(score, scoreBarId, scoreDisplayId) {
  var rake = scoreRake(score);
  G(scoreBarId).style.width = rake + "%";
  var level = getUserLevel(score);
  if (level >= IknowLevel.length) {
    level = IknowLevel.length;
    needscore = "";
  } else {
    needscore = "/" + IknowLevel[level];
  }
  if (score < 0) {
    needscore = "";
  }
  $(".level").text(level)
  G(scoreDisplayId).innerHTML = score + needscore;
}

function G(id) {
  return document.getElementById(id);
}


scoreBar(0, "scoreBar", "scoreDisplay");


async function exp() {
  let res = await fetch('/user')
let json = await res.json()
if (!json.user) {
  return
}
  try {
    let res = await fetch(`/user/exp`)
    let json = await res.json()
    if (200 <= res.status && res.status < 300) {
      scoreBar(json.exp, "scoreBar", "scoreDisplay");
    } else {
      showFormResult({  message: json.message, isSuccess: false })

    }
  } catch (error) {
    showFormResult({ message: error.toString(), isSuccess: false })
  }
}

exp()