let vtube_list = document.querySelector(".vtube_list")
let vtuber_template = vtube_list.querySelector(".vtuber")
vtuber_template.remove()

let updataprofile = document.querySelector("#updataprofile")
let companyList = updataprofile.querySelector("#companyList")

function editVtuber(vtuber) {
  console.log("edit", vtuber.id)
  let fields = ["jp_vtuber_name", "cn_vtuber_name", "group_name"]
  for (let field of fields) {
    updataprofile.querySelector("." + field).value = vtuber[field]
  }
  let form = updataprofile.querySelector("form")
  form.vtuber_group_id.value = vtuber.vtuber_group_id
}

async function delVtuber(vtuber, vtuber_node) {
  console.log("del", vtuber.id)
  // TODO ask user to confirm
  let result = await Swal.fire(
    {
      title: "Confirm to delete " + vtuber.jp_vtuber_name + " ?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
    },

    "",
    "question",
  )
  console.log(result)
  if (!result.isConfirmed) {
    return
  }
  let res = await fetch("/vtuber/" + vtuber.id, { method: "DELETE" })
  let json = await res.json()
  if (json.error) {
    // TODO show error
    Swal.fire("Failed to delete vtuber", json.error, "error")
    return
  }
  Swal.fire("Deleted " + vtuber.jp_vtuber_name, json.error, "error")
  vtuber_node.remove()
}

function showVtuber(vtuber) {
  let vtuber_node = vtuber_template.cloneNode(true)

  let fields = ["id", "jp_vtuber_name", "cn_vtuber_name", "group_name"]

  for (let field of fields) {
    vtuber_node.querySelector("." + field).textContent = vtuber[field]
  }

  vtuber_node.querySelector(".edit_btn").addEventListener("click", () => {
    editVtuber(vtuber)
  })
  vtuber_node.querySelector(".del_btn").addEventListener("click", () => {
    delVtuber(vtuber, vtuber_node)
  })

  vtube_list.appendChild(vtuber_node)

  return

  let div = document.createElement("tr")
  div.className = "vtuber"
  div.innerHTML = /* html */ `<td>${vtuber.id}</td>
<td>${vtuber.jp_vtuber_name}</td>
<td>${vtuber.cn_vtuber_name}</td>
<td>${vtuber.group_name}</td>
<td>
<form action="" method="post">
<input type="hidden" name="edit_id" value="${vtuber.id}" />
<button
  id="row-${vtuber.id}" 
  type="button"
  name="edit_btn"
  class="btn btn-success"
  data-bs-toggle="modal"
  data-bs-target="#updataprofile"
>
  EDIT
</button>
</form>
</td>
<td>
<form action="" method="post">
<input type="hidden" name="delete_id" value="${vtuber.id}" />
<button
  type="button"
  name="edit_btn"
  class="btn btn-danger"
>
  DELETE
</button>
</form>
</td>`
  tbody.appendChild(div)

  document
    .querySelector(`#row-${vtuber.id}`)
    .addEventListener("click", (event) => {
      const id = parseInt(event.currentTarget.id.replace("row-", ""))
      const data = json.filter((item) => item.id === id)
      //  console.log(data[0].en_vtuber_name)
      //  document.querySelector('#jpname').value = data[0].jp_vtuber_name
      //  document.querySelector('#cnname').value = data[0].cn_vtuber_name
      //  document.querySelector('#enname').value = data[0].en_vtuber_name
      //  document.querySelector('#company-edit').value = data[0].group_name
      //  document.querySelector('#period-edit').value = data[0].period
      //  document.querySelector('#pic_link').value = data[0].image
      //  document.querySelector('#twitter').value = data[0].twitter
      //  document.querySelector('#youtube').value = data[0].youtube
      //  document.querySelector('#label').value = data[0].label
    })
}

async function loadVtuber() {
  let res = await fetch("/vtuber")
  json = await res.json()
  console.log(json)
  for (let vtuber of json) {
    showVtuber(vtuber)
  }
}
loadVtuber()

async function loadCompanyList() {
  let res = await fetch("/company")
  let companyRows = await res.json()
  for (let company of companyRows) {
    let option = document.createElement("option")
    option.value = company.group_name
    companyList.appendChild(option)
  }
}
loadCompanyList()
