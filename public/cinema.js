function showFormResult({ form, title, message, isSuccess }) {
    let icon;
    if (isSuccess) {
      form.reset();
      icon = "success";
    } else {
      icon = "error";
    }
    Swal.fire({
      title: title,
      text: message,
      icon: icon,
    });
  }
  




  let artsPlayList = document.querySelector(".artsPlayList");
  function showArtsCard(arts) {
    let div = document.createElement("div");
    div.className = "swiper-slide";
    div.innerHTML = /* html */ `
      <div class="ivu-col ivu-col-span-6">
      <div class="card-frame arts-list-card">
          <div class="loading-frame" id="arts${arts.id}">
          
              ${arts.upfilename ? `<img src="${arts.upfilename}">` : ""}
              
              </div>
              <a href=cinema-details.html?id=${arts.id}>
          <div class="card-frame-hover">
          
          <i class="fas fa-play"></i>
          
        </div>
        </a>
      </div>
      <div class="card-info">
      <div class="arts-info">
        <div class="arts-user-icon">
        ${arts.upfilename ? `<img src="${arts.upfilename}">` : ""}
        </div>
        <div class="arts-title">
          <p class="card-info-title-p">${arts.title}</p>
           <p class="arts-user-name">${arts.artist}</p>
          <p class="arts-frequency">觀看次數: view</p>
        </div>
      </div>
    </div>
    </div>
      `;
    artsPlayList.appendChild(div);
  }
  
  
  async function loadArts() {
    let res = await fetch("/arts");
    let arts = await res.json();
    console.log(arts);
    artsPlayList.innerHTML = "";
    for (let art of arts) {
      showArtsCard(art);
    }
  }
  
  
  let artsSubmit = document.querySelector("form.arts-post-form");
  artsSubmit.addEventListener("submit", async (event) => {
    event.preventDefault();
    let form = artsSubmit;
    let body = new FormData(form);
  
    try {
      let res = await fetch("/artsSubmit", {
        method: "POST",
        body: body,
      });
      let json = await res.json();
      console.log("Show for debug:", json);
  
      if (200 <= res.status && res.status < 300) {
        Swal.fire({
          icon: "success",
          title: "Success",
          text: "Finish upload!",
        });
        loadArts();
      } else {
        Swal.fire({
          icon: "error",
          title: "Failed",
          text: "Upload incomplete!",
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Failed",
        text: "Upload incomplete!",
      });
    }
  });
  


  
  window.onload = async function(){
  await loadArts();
  
  }