let searchParams = new URLSearchParams(location.search)
let id = searchParams.get("id")


function showFormResult({ form, title, message, isSuccess }) {
  let icon;
  if (isSuccess) {
    if(form){
      form.reset();
    }
    icon = "success";
  } else {
    icon = "error";
  }
  Swal.fire({
    title: title,
    text: message,
    icon: icon,
  });
}



// let likeCount = document.querySelector('span.like-count')
let likeButton = document.querySelectorAll(".like")
for (let i=0; i<likeButton.length; i++){
  likeButton[i].addEventListener("click", ()=>{
    likeButton[i].classList.toggle("bg-red");
  })
}

// likeButton.addEventListener("click", async () => {
//   let title = 'like'
//   try {
//     let res = await fetch(`/music/${id}/like`, { method: "POST" })
//     let json = await res.json()
//     let countLike = json.likes
//     if (json.error) {
//       showFormResult({ form, title, message: json.message, isSuccess: false }) 
//     } else {
//       console.log(countLike)
//       checkLike()
//     }
//   } catch (error) {
//     showFormResult({
//       title,
//       message: error.toString(),
//       isSuccess: false,
//     })
//   }
// })



// async function checkLike(){
//   try{
//     let res = await fetch(`/music/${id}/like`)
//     let json = await res.json()
//     let likes = json.likes
//     if(json.error){
//       //show error
//     }else if(json.likes){
//       likeButton.className.add("bg-red")
//     }
//   }catch(error){
//     //show error
//   }

// }



async function loadViewCount(){
  let res = await fetch(`/music/${id}/session`)
  let json = await res.json()
  console.log(json.views)
}

let musicSharePart = document.querySelector(".music-share-part")
function createMusicSharePart(music) {
  let div = document.createElement("div")
  div.className = "display-flex music-details"
  div.innerHTML = /* html */ `
  <div class="top-line">
  <div class="music-name">${music.music_name}</div>
  ${music.singer_name?`<div class="singer">${music.singer_name}</div>`:""}
</div>
<div class="outside-music-photo">
    ${
      music.music_photo
        ? `<img src="${music.music_photo}" class="music-share-photo">`
        : ""
    }
</div>
<audio controls src="${
    music.music_file
  }" type="audio/mpeg" class="music-share-file"></audio>
  `

  musicSharePart.appendChild(div)

  let para = document.createElement("p")
  para.className = "music-share-content"
  para.innerHTML = /*html*/ `${music.music_content}`
  musicSharePart.appendChild(para)
}

let reply = document.querySelector("#comment")
reply.addEventListener("click", () => {
  document.querySelector(".commentInfo").style.display = "block"
})

let postSubmit = document.querySelector("form#comment-text")
postSubmit.addEventListener("submit", async (event) => {
  event.preventDefault()
  let form = event.target
  let formObject = {}
  formObject['comment'] = form.comment.value
  formObject['musicId'] = id
  try {
    const res = await fetch(`/music/${id}/share`, {
      method: "POST",
      headers: {
        "Content-Type":"application/json",
      },
      body: JSON.stringify(formObject),
    })
    let json = await res.json();
    if (200 <= res.status && res.status < 300) {
      showFormResult({ form, message: json.message, isSuccess: true })
      loadComment();
    } else {
      showFormResult({ form, message: "error", isSuccess: false })
    }

  } catch (error) {
    showFormResult({ form, message: "Please type comment", isSuccess: false })
  }
})

let commentList = document.querySelector(".comment-list")
function postComment(post) {
  let date = new Date(post.updated_at)
  let dateOfYear = date.getFullYear()
  let dateOfMonth = date.getMonth() + 1
  let dateOfDay = date.getDate()
  let dateOfHour = date.getHours()
  let dateOfMins = date.getMinutes()
  let li = document.createElement("li")
  li.className = "comments"
  li.innerHTML = /* html */ `
    <div class="one-comment display-flex">
    <div class="user-details">
        <div class="user-icon">
        ${post.icon ? `<img src="${post.icon}">` : "user-icon"}
        </div>
        <div class="username">${post.username}</div>
    </div>
    <div class="comment-details">
        <div class="comment-text music-share-reply">
            <p>${post.content}</p>
        </div>
        <div class="updated-time display-flex">
          <div class="year">${dateOfYear}-</div>
          <div class="month">${dateOfMonth}-</div>
          <div class="day">${dateOfDay}</div>
          <div class="space"></div>
          <div class="hour">${dateOfHour}:</div>
          <div class="mins">${dateOfMins}</div>
        </div>

        <div class="media-area">

        </div>
    </div>
</div>
    `
  commentList.appendChild(li) 
}

async function loadComment() {
  let res = await fetch(`/music/${id}/share`)
  let json = await res.json()
  commentList.innerHTML = ""
  console.log("json:",json.postList)
  for (let post of json.postList) {
    if(post.music_id == id){
      postComment(post)
    }
  }
}

async function loadMusicSharePage() {
  let res = await fetch("/music")
  let json = await res.json()
  musicSharePart.innerHTML = ""
  for (let music of json.musicList) {
    if (music.id == id) {
      createMusicSharePart(music)
    }
  }
}


window.onload = async function () {
  await loadMusicSharePage()
  await loadComment()
  await loadViewCount()
}
