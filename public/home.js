//more
let moremusic = document.querySelector('.more')
moremusic.addEventListener("click", (e) => {
  location.replace("video-more.html")
});


async function loadNewVideo() {
  let res = await fetch("/imageSlider")
  let imageList = await res.json()
  for (let video of videoList) {
    console.log(video)
    showNewVideo(video)
  }
}






//image slider
class ImageCarousel {
  static DEFAULT_OPTIONS = {
    startingIndex: 0,
    preloadImages: true,
    showControls: true,
    showIndicators: true,
    autoPlay: true,
    slideDurationMs: 10000
  };

  static CAROUSEL_CLASS = "carousel";
  static IMAGE_SLIDER_CLASS = "imageSlider";
  static IMAGE_CLASS = "image";
  static CONTROLS_CLASS = "controls";
  static LEFT_ARROW_CLASS = "leftArrow";
  static RIGHT_ARROW_CLASS = "rightArrow";
  static PROGRESS_BAR_CLASS = "progressBar";
  static PROGRESS_FILL_CLASS = "progressFill";
  static INDICATOR_ROW_CLASS = "indicatorRow";
  // static BANNER_ROW_CLASS = "banners";
  static INDICATOR_CLASS = "indicator";

  static SELECTED_CLASS = "selected";

  constructor(images, options = {}) {
    this.images = images;
    this.options = {...ImageCarousel.DEFAULT_OPTIONS, ...options };
    this.currentIndex = this.options.startingIndex;

    this._elems = {};

    // autoPlay properties
    if (this.options.autoPlay) {
      this._slideshowStartTime = null;
      this._slideshowPausePercentage = 0;
    }

    if (this.options.preloadImages) {
      this._preloadImagesDone = false;
      this._preloadImagesPromise = this._preloadImages();
    }
  }

  async render(container) {
    // If still waiting on preloading images, wait till it finishes
    if (this.options.preloadImages && !this._preloadImagesDone) {
      await this._preloadImagesPromise;
    }

    while (container.firstChild) {
      container.removeChild(container.firstChild);
    }
    container.classList.add(ImageCarousel.CAROUSEL_CLASS);

    this._elems.container = container;

    this._renderImageSlider();

    if (this.options.showControls) {
      this._renderControls();
      this._bindControlsEvents();
    }

    if (this.options.autoPlay) {
      this._renderProgressBar();
      this._handleAutoPlay();
    }

    if (this.options.showIndicators) {
      this._renderIndicators();
      this._bindIndicatorsEvents();
    }
    // if (this.options.showBanners) {
    //     this._renderBanner();
    // }
  }

  _preloadImages() {
    const preload = (url) => {
      return new Promise(resolve => {
        const img = new Image();
        img.onload = resolve;
        img.onerror = resolve;
        img.src = url;
      });
    };

    return Promise
      .all(this.images.map(imgUrl => preload(imgUrl)))
      .then(res => {
        this._preloadImagesDone = true;
        return Promise.resolve(true);
      });
  }

  _renderImageSlider() {
    const imageSlider = document.createElement("div");
    imageSlider.classList.add(ImageCarousel.IMAGE_SLIDER_CLASS);

    this._elems.images = [];
    for (let i = 0; i < this.images.length; i++) {
      const image = document.createElement("img");
      image.src = this.images[i];
      image.dataset.index = i;
      image.classList.add(ImageCarousel.IMAGE_CLASS);
      if (i === this.currentIndex) {
        image.classList.add(ImageCarousel.SELECTED_CLASS);
      }

      this._elems.images.push(image);

      imageSlider.appendChild(image);
    }

    this._elems.imageSlider = imageSlider;
    this._elems.container.appendChild(imageSlider);
  }

  _renderControls() {
    const controls = document.createElement("div");
    controls.classList.add(ImageCarousel.CONTROLS_CLASS);

    const leftArrow = document.createElement("div");
    leftArrow.classList.add(ImageCarousel.LEFT_ARROW_CLASS);
    leftArrow.innerHTML = "&#8249;"; //"&larr;"; // left arrow

    const rightArrow = document.createElement("div");
    rightArrow.classList.add(ImageCarousel.RIGHT_ARROW_CLASS);
    rightArrow.innerHTML = "&#8250;"; //"&rarr;"; // right arrow

    this._elems.controls = controls;
    this._elems.leftArrow = leftArrow;
    this._elems.rightArrow = rightArrow;

    controls.appendChild(leftArrow);
    controls.appendChild(rightArrow);
    this._elems.imageSlider.appendChild(controls);
  }

  _renderProgressBar() {
    const progressBar = document.createElement("div");
    progressBar.classList.add(ImageCarousel.PROGRESS_BAR_CLASS);

    const progressFill = document.createElement("div");
    progressFill.classList.add(ImageCarousel.PROGRESS_FILL_CLASS);

    progressBar.appendChild(progressFill);
    this._elems.container.appendChild(progressBar);

    this._elems.progressBar = progressBar;
    this._elems.progressFill = progressFill;
  }

  _renderIndicators() {
      const indicatorRow = document.createElement("div");
      indicatorRow.classList.add(ImageCarousel.INDICATOR_ROW_CLASS);

      this._elems.indicators = [];
      for (let i = 0; i < this.images.length; i++) {
        const indicator = document.createElement("span");
        indicator.dataset.index = i;
        indicator.classList.add(ImageCarousel.INDICATOR_CLASS);
        if (i === this.currentIndex) {
          indicator.classList.add(ImageCarousel.SELECTED_CLASS);
        }

        this._elems.indicators.push(indicator);

        indicatorRow.appendChild(indicator);
      }

      this._elems.indicatorRow = indicatorRow;

      this._elems.container.appendChild(indicatorRow);
    }
    // _renderBanner() {
    //     const banners = document.createElement("div");
    //     banners.classList.add(ImageCarousel.BANNER_ROW_CLASS);

  //     this._elems.bannerss = [];
  //     for (let i = 0; i < this.images.length; i++) {
  //         const banners = document.createElement("span");
  //         banners.dataset.index = i;
  //         banners.classList.add(ImageCarousel.BANNER_CLASS);
  //         if (i === this.currentIndex) {
  //             banners.classList.add(ImageCarousel.SELECTED_CLASS);
  //         }

  //         this._elems.bannerss.push(banners);

  //         banners.appendChild(banners);
  //     }

  //     this._elems.banners = banners;

  //     this._elems.container.appendChild(banners);
  // }

  _bindControlsEvents() {
    this._elems.leftArrow.addEventListener("click", this._prevImage.bind(this));
    this._elems.rightArrow.addEventListener(
      "click",
      this._nextImage.bind(this)
    );
  }

  _bindIndicatorsEvents() {
      for (let indicator of this._elems.indicators) {
        indicator.addEventListener("click", (e) => {
          this._changeImage(parseInt(indicator.dataset.index));
        });
      }
    }
    // _bindBannerssEvents() {
    //     for (let banners of this._elems.bannerss) {
    //         banners.addEventListener("click", (e) => {
    //             this._changeImage(parseInt(banners.dataset.index));
    //         });
    //     }
    // }

  _handleAutoPlay() {
    const progress = this._elems.progressFill;
    let shouldPause = false;

    this._elems.container.addEventListener("mouseover", (e) => {
      shouldPause = true;
    });

    this._elems.container.addEventListener("mouseout", (e) => {
      shouldPause = false;
      this._slideshowStartTime = performance.now();
      window.requestAnimationFrame(stepFunc);
    });

    const stepFunc = (timestamp) => {
      // On very first frame
      if (!this._slideshowStartTime) {
        this._slideshowStartTime = timestamp;
      }

      // "elapsed" cycles back to 0 after slide duration
      const elapsed =
        (timestamp - this._slideshowStartTime) % this.options.slideDurationMs;

      // Calculates percentage in the slideshow
      // If user hover in and out, the animation resumes.
      // The elapsed would start back at "0" since the
      // this._slideshowStartTime is reset to now().
      // But add in the saved percentage to continue from before.
      const percentage =
        elapsed / this.options.slideDurationMs + this._slideshowPausePercentage;

      // If user hovers on the carousel, pause the animation
      // and save current percentage.
      if (shouldPause) {
        this._slideshowPausePercentage = percentage;
        return;
      }

      // Easing func to make the progress bar animation more appealing
      // than plain linear. Formula from https://easings.net/#easeInOutSine
      const easingFunc = (num) => -(Math.cos(Math.PI * num) - 1) / 2;

      // Update progress fill bar
      progress.style.width = `${100 * easingFunc(percentage)}%`;

      if (percentage > 0.99) {
        // 99% as it's rarely actually 100%
        this._slideshowPausePercentage = 0;
        this._nextImage(); // this._slideshowStartTime is reset
      }

      window.requestAnimationFrame(stepFunc);
    };

    // Initial function call
    window.requestAnimationFrame(stepFunc);
  }

  _prevImage() {
    const newIndex =
      this.currentIndex > 0 ? this.currentIndex - 1 : this.images.length - 1;
    this._changeImage(newIndex);
  }

  _nextImage() {
    const newIndex =
      this.currentIndex < this.images.length - 1 ? this.currentIndex + 1 : 0;
    this._changeImage(newIndex);
  }

  _changeImage(index) {
    const currImage = this._elems.imageSlider.querySelector(
      `[data-index="${this.currentIndex}"]`
    );
    const newImage = this._elems.imageSlider.querySelector(
      `[data-index="${index}"]`
    );

    currImage.classList.remove(ImageCarousel.SELECTED_CLASS);
    newImage.classList.add(ImageCarousel.SELECTED_CLASS);

    if (this.options.showIndicators) {
      const currIndicator = this._elems.indicatorRow.querySelector(
        `[data-index="${this.currentIndex}"]`
      );
      const newIndicator = this._elems.indicatorRow.querySelector(
        `[data-index="${index}"]`
      );

      currIndicator.classList.remove(ImageCarousel.SELECTED_CLASS);
      newIndicator.classList.add(ImageCarousel.SELECTED_CLASS);
    }
    // if (this.options.showBanners) {
    //     const currBanners = this._elems.banners.querySelector(
    //         `[data-index="${this.currentIndex}"]`
    //     );
    //     const newBanners = this._elems.banners.querySelector(
    //         `[data-index="${index}"]`
    //     );

    //     currBanners.classList.remove(ImageCarousel.SELECTED_CLASS);
    //     newBanners.classList.add(ImageCarousel.SELECTED_CLASS);
    // }

    // restart the slideshow progress
    if (this.options.autoPlay) {
      this._elems.progressFill.style.width = "0%";
      this._slideshowPausePercentage = 0;
      this._slideshowStartTime = performance.now();
    }

    this.currentIndex = index;
  }
}

const images = [
  "https://assets.codepen.io/6124938/Konachan.com+-+267367.jpg",
  "https://assets.codepen.io/6124938/Konachan.com+-+307514.jpg",
  "https://assets.codepen.io/6124938/Konachan.com+-+66171.jpg",
  "https://assets.codepen.io/6124938/Konachan.com+-+294174.jpg",
  "https://assets.codepen.io/6124938/Konachan.com+-+201232.jpg"
];
const imageTitles = [
  "123",
  "333",
  "256",
  "777",
  "888"
];
const imageLinks = [
  "https://assets.codepen.io/6124938/Konachan.com+-+267367.jpg",
  "https://assets.codepen.io/6124938/Konachan.com+-+307514.jpg",
  "https://assets.codepen.io/6124938/Konachan.com+-+66171.jpg",
  "https://assets.codepen.io/6124938/Konachan.com+-+294174.jpg",
  "https://assets.codepen.io/6124938/Konachan.com+-+201232.jpg"
];

const imageCarousel = new ImageCarousel(images, {
  startingIndex: 1,
  preloadImages: true,
  showControls: true,
  showIndicators: true,
  autoPlay: true,
  // showBanners: true,
  slideDurationMs: 7500
});

async function initImageContainer() {
  let imageContainer = document.getElementById("container")
  await imageCarousel.render(imageContainer);


  console.log(imageContainer.querySelectorAll('img'));
  imageContainer.querySelectorAll('img').forEach((img, i) => {
    console.log(img);
    img.addEventListener('click', () => {
      console.log('open', imageLinks[i]);
      window.open(imageLinks[i])
    })
  })
  imageContainer.querySelector('.imageSlider').addEventListener('click', (event) => {
    console.log('check', event.target);
    if (event.target.className != 'controls') return
    let img = imageContainer.querySelector('img.selected')
    let imgs = Array.from(imageContainer.querySelectorAll('img'))
    let i = imgs.indexOf(img)
    console.log('open', imageLinks[i]);
    window.open(imageLinks[i])
  })


  let imageTitle = document.createElement('div')
  imageTitle.className = 'banners'
  imageTitle.textContent = ''
  imageContainer.appendChild(imageTitle)

  let imageSlider = imageContainer.querySelector('.imageSlider')

  // image.addEventListener('click', () => {
  //             location.replace("video-more.html")
  // })
  function checkTitle() {
    let img = imageSlider.querySelector('.selected')
    if (!img) {
      return
    }
    let index = images.indexOf(img.src)
    let title = imageTitles[index]
    imageTitle.textContent = title
  }
  checkTitle()
  let observer = new MutationObserver(() => {
    checkTitle()
  });
  observer.observe(imageSlider, {
    subtree: true,
    attributes: true
  });
}
initImageContainer()


//最新

const controls = document.querySelector(".controls");
const container = document.querySelector(".swiper-wrap");
const allBox = container.children;
const containerWidth = container.offsetWidth;
const margin = 10;
var totalItems = 0;
var jumpSlideWidth = 0;
var items = 0;

// item setup per slide


function load() {

  responsive = [
    { breakPoint: { width: 0, item: 2 } }, //if width greater than 0 (1 item will show) 
    { breakPoint: { width: 600, item: 3 } }, //if width greater than 600 (2  item will show) 
    { breakPoint: { width: 1000, item: 4 } } //if width greater than 1000 (4 item will show) 
  ]
  for (let i = 0; i < responsive.length; i++) {
    if (window.innerWidth > responsive[i].breakPoint.width) {
      items = responsive[i].breakPoint.item
    }
  }
  var totalItemsWidth = 0
  for (let i = 0; i < allBox.length; i++) {
    // width and margin setup of items
    allBox[i].style.width = (containerWidth / items) - margin + "px";
    allBox[i].style.margin = (margin / 2) + "px";
    totalItemsWidth += containerWidth / items;
    totalItems++;
  }

  // thumbnail-container width set up
  container.style.width = totalItemsWidth + "px";

  // slides controls number set up
  const allSlides = Math.ceil(totalItems / items);
  const ul = document.createElement("ul");
  for (let i = 1; i <= allSlides; i++) {
    const li = document.createElement("li");
    li.id = i;
    li.innerHTML = i;
    li.setAttribute("onclick", "controlSlides(this)");
    ul.appendChild(li);
    if (i == 1) {
      li.className = "active";
    }
  }
  controls.appendChild(ul);
}

// when click on numbers slide to next slide
function controlSlides(ele) {
  // select controls children  'ul' element 
  const ul = controls.children;

  // select ul children 'li' elements;
  const li = ul[0].children


  var active;

  for (let i = 0; i < li.length; i++) {
    if (li[i].className == "active") {
      // find who is now active
      active = i;
      // remove active class from all 'li' elements
      li[i].className = "";
    }
  }
  // add active class to current slide
  ele.className = "active";

  var numb = (ele.id - 1) - active;
  jumpSlideWidth = jumpSlideWidth + (containerWidth * numb);
  container.style.marginLeft = -jumpSlideWidth + "px";
}

let latestIndex = 0

function slideLatestRight() {
  let itemPerPage = 4
  latestIndex += itemPerPage
  slideLatest(latestIndex)
}

function slideLatestLeft() {
  let itemPerPage = 4
  latestIndex -= itemPerPage
  slideLatest(latestIndex)
}

function slideLatest(index) {
  let itemPerPage = 4
  let itemCount = 5
  if (index > itemCount - itemPerPage) {
    index = itemCount - itemPerPage
  }
  if (index < 0) {
    index = 0
  }
  // var active = 1
  // var numb = (ele.id - 1) - active;
  var numb = index / 4
  var jumpSlideWidth = (containerWidth * numb);
  container.style.marginLeft = -jumpSlideWidth + "px";
}



//人氣Vtuber
const controls2 = document.querySelector(".controls2");
const container2 = document.querySelector(".swiper-wrapper-vtube");
const allBox2 = container2.children;
const containerWidth2 = container2.offsetWidth;
const margin2 = 10;
var items2 = 0;
var totalItems2 = 0;
var jumpSlideWidth2 = 0;


// item setup per slide


function load2() {
  responsive2 = [
    { breakPoint: { width: 0, item: 2 } }, //if width greater than 0 (1 item will show) 
    { breakPoint: { width: 600, item: 4 } }, //if width greater than 600 (2  item will show) 
    { breakPoint: { width: 1000, item: 6 } } //if width greater than 1000 (4 item will show) 
  ]
  for (let i = 0; i < responsive2.length; i++) {
    if (window.innerWidth > responsive2[i].breakPoint.width) {
      items2 = responsive2[i].breakPoint.item
    }
  }
  var totalItemsWidth = 0
  for (let i = 0; i < allBox2.length; i++) {
    // width and margin setup of items
    allBox2[i].style.width = (containerWidth2 / items2) - margin2 + "px";
    allBox2[i].style.margin = (margin2 / 2) + "px";
    totalItemsWidth += containerWidth2 / items2;
    totalItems2++;
  }

  // thumbnail-container width set up
  container2.style.width = totalItemsWidth + "px";

  // slides controls number set up
  const allSlides2 = Math.ceil(totalItems2 / items2);
  const ul = document.createElement("ul");
  for (let i = 1; i <= allSlides2; i++) {
    const li = document.createElement("li");
    li.id = i;
    li.innerHTML = i;
    li.setAttribute("onclick", "controlSlides2(this)");
    ul.appendChild(li);
    if (i == 1) {
      li.className = "active";
    }
  }
  controls2.appendChild(ul);
}

// when click on numbers slide to next slide
function controlSlides2(ele) {
  // select controls children  'ul' element 
  const ul = controls2.children;

  // select ul children 'li' elements;
  const li = ul[0].children


  var active;

  for (let i = 0; i < li.length; i++) {
    if (li[i].className == "active") {
      // find who is now active
      active = i;
      // remove active class from all 'li' elements
      li[i].className = "";
    }
  }
  // add active class to current slide
  ele.className = "active";

  var numb = (ele.id - 1) - active;
  jumpSlideWidth2 = jumpSlideWidth2 + (containerWidth2 * numb);
  container2.style.marginLeft = -jumpSlideWidth2 + "px";
}



//計算歌曲
// const musiccontainer = document.querySelector(".music");
// const musicBox = musiccontainer.children;
// const musicWidth = musiccontainer.offsetWidth;
// const musicmargin = 12;
// var musicitems = 0;

// function load3() {

//     responsive = [
//         { breakPoint: { width: 0, item: 1 } }, //if width greater than 0 (1 item will show) 
//         { breakPoint: { width: 600, item: 2 } }, //if width greater than 600 (2  item will show) 
//         { breakPoint: { width: 1000, item: 3 } } //if width greater than 1000 (4 item will show) 
//     ]
//     for (let i = 0; i < responsive.length; i++) {
//         if (window.innerWidth > responsive[i].breakPoint.width) {
//             musicitems = responsive[i].breakPoint.item
//         }
//     }
//     var totalItemsWidth = 0
//     for (let i = 0; i < musicBox.length; i++) {
//         // width and margin setup of items
//         musicBox[i].style.width = (musicWidth / musicitems) - musicmargin + "px";
//         musicBox[i].style.margin = (musicmargin / 2) + "px";
//         totalItemsWidth += musicWidth / musicitems;
//         totalItems++;
//     }

//     // thumbnail-container width set up
//     musiccontainer.style.width = totalItemsWidth + "px";
// }




let groupDisplay = document.querySelector('.swiper-wrapper')
let vtuberDisplay = groupDisplay.querySelector('.swiper-slide-vtube')
vtuberDisplay.remove()


async function loadGroups() {
  let res = await fetch(`/home/vtuber`)
  let json = await res.json()
  for (let vtuber of json.vtuber) {
    console.log(vtuber)
    showVtuber(vtuber)
  }
}

async function showVtuber(vtuber) {
  let VtuberItem = vtuberDisplay.cloneNode(true)
  VtuberItem.querySelector(".vtuber-link").href += "?id=" + vtuber.id
  VtuberItem.querySelector("img").src = vtuber.image
  VtuberItem.querySelector(".card-info-title-p").textContent = vtuber.jpname
  groupDisplay.appendChild(VtuberItem)
}




async function loadNewVideo() {
  let res = await fetch("/video")
  let videoRows = await res.json()
  videoList = videoRows.videoList.slice(0, 8)
  for (let video of videoList) {
    console.log(video)
    showNewVideo(video)
  }
}


let newVideoDisplay = document.querySelector('.video-new')
let videoDisplay = newVideoDisplay.querySelector('.slide-video')
videoDisplay.remove()

async function showNewVideo(video) {
  let VideoItem = videoDisplay.cloneNode(true)
  VideoItem.querySelector(".video_new_url").href += "?id=" + video.id

  VideoItem.querySelector(".video_new_title").textContent = video.title

  if (video.image) {
    VideoItem.querySelector("img").src = video.image
  }
  newVideoDisplay.appendChild(VideoItem)
}



async function loadHotVideo() {
  let res = await fetch("/home/video")
  let videoRows = await res.json()
  for (let video of videoRows) {
    showHotVideo(video)
  }
}


let hotVideoDisplay = document.querySelector('.home-hot-scroll')
let videoDisplayRight = hotVideoDisplay.querySelector('.home-hot-frame')
videoDisplayRight.remove()

async function showHotVideo(video) {
  let VideoItem = videoDisplayRight.cloneNode(true)
  VideoItem.querySelector(".hot-url").href += "?id=" + video.id

  VideoItem.querySelector(".hot-title").textContent = video.title

  if (video.image) {
    VideoItem.querySelector("img").src = video.image
  }
  hotVideoDisplay.appendChild(VideoItem)
}





async function loadAll() {
  await Promise.all([loadGroups(), loadNewVideo()])
  await Promise.all([load(), load2()])
}

loadAll()
loadHotVideo()