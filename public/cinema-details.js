let artsDetails = document.querySelector(".main-body")
let params = new URLSearchParams(location.search)
let id = params.get("id")

async function loadDetails() {
    let res = await fetch('/arts/' + id)
    let json = await res.json()
    if (json.error) {
        console.log(json.error)
    } else {
        showDetails(json.art)
    }


}
loadDetails()


function showDetails(art) {
    let div = document.createElement("div");
    div.className = "home-bac";
    div.innerHTML = /* html */ `
      <div id ="title">
        <h2>${art.title}</h2>
      </div>
      <div id ="drawing">
       <img src ="${art.upfilename}">
      </div>
      <div id ="artist">
       <h3>${art.artist}</h3>
      </div>
      <div id ="introduction">
       <p>${art.intro}</p>
      </div>
      <div id ="comment">
  
      </div>
      `;
      artsDetails.appendChild(div);

}