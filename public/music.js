/* Header */
// window.onscroll = () => {
//   if (window.scrollY > 100) {
//     document.querySelector("header").classList.add("header-active");
//   } else {
//     document.querySelector("header").classList.remove("header-active");
//   }
// };


// let user_login = document.querySelector(".user-control-wrap");
// user_login.addEventListener("click", () => {
//   location.href = "/login.html";
// });

// $(".user-logged-wrap .user-icon").click(function () {
//   $(this).parent().toggleClass("active");
//   $(".ivu-poptip").removeClass("active");
// });

// $(".ivu-poptip .ivu-badge").click(function () {
//   $(this).parent().toggleClass("active");
//   $(".user-logged-wrap").removeClass("active");
// });

// $(".show_all .link").click(function () {
//   $(".ivu-poptip").removeClass("active");
//   $(".popup").show();
// });

// $(".close, .shadow").click(function () {
//   $(".popup").hide();
// });

// window.addEventListener("pageshow", (event) => {
//   var historyTraversal =
//     event.persisted ||
//     (typeof window.performance != "undefined" &&
//       window.performance.navigation.type === 2);
//   if (historyTraversal) {
//     // Handle page restore.
//     window.location.reload();
//   }
// });

// async function logout() {
//   let form = logoutForm;
//   let title = "logout";
//   try {
//     let res = await fetch(form.action, {
//       method: form.method,
//     });
//     let json = await res.json();
//     if (200 <= res.status && res.status < 300) {
//       showFormResult({ form, title, message: json.message, isSuccess: true });
//       await loadUser();
//       // Swal.fire({
//       //     title: 'Logout',
//       //     text: json.message,
//       //     icon: 'success',
//       // })
//     } else {
//       showFormResult({ form, title, message: json.message, isSuccess: false });
//     }
//   } catch (error) {
//     showFormResult({
//       form,
//       title,
//       message: error.toString(),
//       isSuccess: false,
//     });
//   }
// }

// logoutForm.addEventListener('submit', async event => {
//         event.preventDefault()
//         logout()
//     })
/*above is Amen things*/

function showFormResult({ form, title, message, isSuccess }) {
  let icon;
  if (isSuccess) {
    if(form){
      form.reset();
    }
    icon = "success";
  } else {
    icon = "error";
  }
  Swal.fire({
    title: title,
    text: message,
    icon: icon,
  });
}

let musicPlayList = document.querySelector(".musicPlayList");
function showMusicCard(music) {
  let div = document.createElement("div");
  div.className = "swiper-slide";
  div.innerHTML = /* html */ `
    <div class="ivu-col ivu-col-span-6">
    <div class="card-frame music-list-card">
        <div class="loading-frame" id="music${music.id}">
        
            ${music.music_photo ? `<img src="${music.music_photo}">` : ""}
            
            </div>
            <a href=music-share.html?id=${music.id}>
        <div class="card-frame-hover">
        
        <i class="fas fa-play"></i>
        
      </div>
      </a>
    </div>
    <div class="card-info">
    <div class="music-info">
      <div class="music-user-icon">
      ${music.icon ? `<img src="${music.icon}">` : ""}
      </div>
      <div class="music-title">
        <p class="card-info-title-p">${music.music_name}</p>
        <p class="music-user-name">${music.username}</p>
        <p class="music-frequency" id="viewOf${music.id}">No. of View:${music.views}</p>
      </div>
    </div>
  </div>
  </div>
    `;
  musicPlayList.appendChild(div);
}

let singerPlayList = document.querySelector(".singerPlayList");
function showSingerCard(singer) {
  let div = document.createElement("div");
  div.className = "swiper-slide";
  div.innerHTML = /* html */ `
    <div class="ivu-col ivu-col-span-6">
    <div class="card-frame music-list-card">

      
        <div class="loading-frame" id="singer${singer.id}">
        <div class="singer-name">${singer.singer_name}</div>
        </div>   

            <a href=singer.html?singerId=${singer.id}>
        <div class="card-frame-hover">
        
        <i class="fas fa-play"></i>
        
      </div>
      </a>
    </div>
    <div class="card-info">
    <div class="music-info">
      <div class="music-title">
        <p class="music-frequency" id="viewOf${singer.id}">No. of View:${singer.singerviews}</p>
      </div>
    </div>
  </div>
  </div>
    `;
  singerPlayList.appendChild(div);
}

async function loadSinger(){
  let res = await fetch("/music-singer");
  let json = await res.json();
  // console.log(json.singerList)
  singerPlayList.innerHTML = "";
  for (let singer of json.singerList) {
    if(singer.singer_name != "")
    showSingerCard(singer);
  }
}

async function loadMusic() {
  let res = await fetch("/music");
  let json = await res.json();
  musicPlayList.innerHTML = "";
  for (let music of json.musicList) {
    showMusicCard(music);
  }
}

// async function loadViewCount(){
//   let res = await fetch(`/music/${id}/session`)
//   let json = await res.json()
//   console.log(json.views)
//   // let musicViewCount = document.querySelectorAll('.music-frequency')
//   // musicViewCount.textContent = json.views
//   document.querySelector('span.view-count').textContent = `No of view: ${json.views}` 
// }

let musicSubmit = document.querySelector("form.music-post-form");
musicSubmit.addEventListener("submit", async (event) => {
  event.preventDefault();
  let form = musicSubmit;
  let body = new FormData(form);

  try {
    let res = await fetch("/music", {
      method: "POST",
      body: body,
    });
    let json = await res.json();

    if (200 <= res.status && res.status < 300) {
      showFormResult({ form, message: json.message, isSuccess: true })
      loadMusic();
      loadSinger();
    } else {
      showFormResult({ form, message: json.message, isSuccess: false })
    }
  } catch (error) {
    showFormResult({ form, message: json.message, isSuccess: false })
  }
});

// let musicId = document.querySelector(`#music${music.id}`);
// function linkToMusicSharePage() {
//   musicId.addEventListener("click", async (event) => {});
// }

window.onload = async function(){
await loadMusic();
await loadSinger();
}