async function loadVTuberSpace() {
  let params = new URLSearchParams(location.search)
  let id = params.get("id")
  let res = await fetch("/vtuber/" + id)
  let json = await res.json()
  if (json.error) {
    location.href = '/'
    return
  }
  let vtuber = json.vtuber
  showVtuber(vtuber)
  console.log(vtuber)
  loadNewVideo(vtuber)
}
loadVTuberSpace()

let vtube_list = document.querySelector(".vtuber-pc-left")
let vtuber_template = vtube_list.querySelector(".vtuber-pc-info")
let link_template = vtube_list.querySelector(".vtuber-pc-control")

// vtuber_template.remove()

function showVtuber(vtuber) {
  // let vtuber_node = vtuber_template.cloneNode(true)
  let fields = ["jp_vtuber_name", "cn_vtuber_name", "group_name"]
  for (let field of fields) {
    vtuber_template.querySelector("." + field).textContent = vtuber[field]
  }
  if (vtuber.introduction) {
    document.querySelector(".introduction").textContent = vtuber.introduction
  }
  vtube_list.querySelector(".image").src = vtuber.image
    // vtube_list.appendChild(vtuber_node)

  fields = ["bilibili", "youtube", "twitter"]
  for (let field of fields) {
    link_template.querySelector("." + field).href = vtuber[field]
  }
  if (vtuber.url_3d) {
    document.querySelector(".url3D").addEventListener('click', () => {
      open3D(vtuber)
    })
  } else {
    document.querySelector(".url3D").setAttribute("hidden", "")
  }

  return
}

let header = document.querySelector(".header-left")
let hearderImg = header.querySelector("img").remove()

function showback() {
  let div = document.createElement("div")
  div.className = "layout-back"
  div.innerHTML = /* html */ `
    <div class="button-pc-animate" onclick="history.back()">
    <i class="fas fa-chevron-left"></i></div>`
  header.appendChild(div)
}

showback()

let vtuberTab = document.querySelectorAll(".vtuber-pic-tab-pane")
let musicList = document.querySelector(".song-list")
let videoList = document.querySelector(".video-list")
let infoList = document.querySelector(".info-list")

vtuberTab[0].addEventListener("click", () => {
  videoList.removeAttribute("hidden")
  musicList.setAttribute("hidden", "")
  infoList.setAttribute("hidden", "")
})
vtuberTab[1].addEventListener("click", () => {
  videoList.setAttribute("hidden", "")
  musicList.removeAttribute("hidden")
  infoList.setAttribute("hidden", "")
})
vtuberTab[2].addEventListener("click", () => {
  videoList.setAttribute("hidden", "")
  musicList.setAttribute("hidden", "")
  infoList.removeAttribute("hidden")
})

// vtuberTab.forEach(tag =>{
// 	tag.onclick =() =>{
// 		vtuberTab.forEach(list => list.classList.remove('active'));
// 		tag.classList.add('active');
// 		if(video.classList.contains('active')){
// 			let src =tag.children[0].getAttribute('src')
// 			mainVideo.src = src;
// 			let text = tag.children[1].innerHTML;
// 			title.innerHTML = text;
// 		};
// 	};
// });

let params = new URLSearchParams(location.search)
let id = params.get("id")

let likeButton = document.querySelector(".ring-like")
likeButton.addEventListener("click", async() => {
  let title = "like"

  try {
    let res = await fetch(`/vtuber/${id}/like`, {
      method: hasLiked ? "DELETE" : "POST",
    })
    let json = await res.json()
    console.log('json:', json);
    let likeNumber = json.likes
    if (json.error) {
      showFormResult({ title, message: json.message, isSuccess: false })
    } else {
      console.log(likeNumber)
        // likeCount(likeNumber)
      hasLiked = !hasLiked
      if (hasLiked) {
        unlike.setAttribute("hidden", "")
        like.removeAttribute("hidden")
      } else {
        unlike.removeAttribute("hidden")
        like.setAttribute("hidden", "")
      }
    }
  } catch (error) {
    showFormResult({
      // form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
})

/** like 未做到 */
let unlike = document.querySelector(".unlike")
let like = document.querySelector(".like")
let hasLiked
let title = 'like'
async function checkLike() {
  let res = await fetch("/user")
  let json = await res.json()
  if (!json.user) {
    return
  }
  try {
    let res = await fetch(`/vtuber/${id}/like`)
    json = await res.json()
    hasLiked = json.hasLiked
    if (json.error) {
      // showFormResult({ title, message: json.message, isSuccess: false })
    } else if (json.hasLiked) {
      unlike.setAttribute("hidden", "")
      like.removeAttribute("hidden")
    } else {
      unlike.removeAttribute("hidden")
      like.setAttribute("hidden", "")
    }
  } catch (error) {
    showFormResult({
      // form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
}
checkLike()


function open3D(vtuber) {
  Swal.fire({
    width: 1000,
    html: ` <iframe title="Hololive Vtuber" width=100% height=600px frameborder="0" src="${vtuber.url_3d}"> </iframe>`,
  })
}



async function loadNewVideo(vtuber) {
  let list = vtuber.video_list
  for (let video of list) {
    console.log(video)
    showNewVideo(video)
  }
}


let newVideoDisplay = document.querySelector('.video-list')
let videoDisplay = newVideoDisplay.querySelector('.vid2')
videoDisplay.remove()

async function showNewVideo(video) {
  let VideoItem = videoDisplay.cloneNode(true)
  VideoItem.querySelector("a").href += "?id=" + video.id

  VideoItem.querySelector(".title").textContent = video.title

  if (video.image) {
    VideoItem.querySelector("img").src = video.image
  }
  newVideoDisplay.appendChild(VideoItem)
}
