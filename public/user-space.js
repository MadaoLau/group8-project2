async function loadUserSpace() {
  let params = new URLSearchParams(location.search)
  let id = params.get("id")
  let res = await fetch("/member/" + id)
  let json = await res.json()
  if (json.error) {
    location.href = '/'
    return
  }
  let user = json.user
    // console.log(user)

  let fields = ["username"]
  for (let field of fields) {
    showUsername.querySelector("." + field).textContent = user[field]
  }
  console.log(user.username)
  if (user.icon) {
    showUserIcon.src = user.icon
  }
}
loadUserSpace()

let showUsername = document.querySelector(".show-username")
let uploadPreview = document.querySelector(".upload-preview")
let showUserIcon = uploadPreview.querySelector(".usericon")

let uploadUserIcon = document.querySelector(".ivu-upload-input")
let tooltipPopper = document.querySelector(".ivu-tooltip-popper")

uploadPreview.addEventListener("click", () => {
  uploadUserIcon.click()
  uploadicon()
})

uploadPreview.addEventListener("mouseover", () => {
  tooltipPopper.style.display = "block"
})
uploadPreview.addEventListener("mouseout", () => {
  tooltipPopper.style.display = "none"
})

// function showUser(user) {

// }

async function loadUserIcon() {
  let res = await fetch("/user")
  let json = await res.json()
  let params = new URLSearchParams(location.search)
  let id = params.get("id")
  if (!json.user) {
    document.querySelector(".ivu-tooltip-dark").setAttribute("hidden", "")
    document.querySelector(".fa-plus").setAttribute("hidden", "")
    document.querySelector(".mask").style = "background : inherit;"
    return
  }
  if (json.user.id == id) {
    document.querySelector(".ivu-tooltip-dark").removeAttribute("hidden")
    document.querySelector(".fa-plus").removeAttribute("hidden")
  }
}

loadUserIcon()

let adduserForm = document.querySelector("#upload-usericon-form")

function uploadicon() {
  adduserForm.icon_file.addEventListener("change", async() => {
    let file = adduserForm.icon_file.files[0]
    if (!file) return
    readFile(file, (dataUrl) => {
      showUserIcon.src = dataUrl
    })
    let result = await Swal.fire({
        title: "Confirm to change your icon",
        icon: "warning",
        html: "",
        showCancelButton: true,
      },

      "",
      "question",
    )
    console.log(result.isConfirmed) //isConfirmed
    if (!result.isConfirmed) {
      loadUserSpace()
      return
    }
    // adduserForm.submit()
    sendIcon()
  })
}

function readFile(file, cb) {
  let reader = new FileReader()
  reader.onload = () => {
    cb(reader.result)
  }
  reader.readAsDataURL(file)
}

/*** 上傳icon 失敗***/
adduserForm.addEventListener("submit", async(event) => {
  event.preventDefault()
  sendIcon()
})

async function sendIcon() {
  let res = await fetch("/user")
  let json = await res.json()
  let id = json.user.id
  let title = "Icon update"
  let form = adduserForm
  body = new FormData(form)
  try {
    let res = await fetch("/user/icon/" + id, {
      method: form.method,
      body,
    })
    let json = await res.json()
      // console.log('create memo json:', json)
    if (200 <= res.status && res.status < 300) {
      showFormResult({ form, title, message: json.message, isSuccess: true })
    } else {
      showFormResult({ form, title, message: json.message, isSuccess: false })
    }
  } catch (error) {
    showFormResult({
      form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
}


let userVtuber = document.querySelector('.vtuberlike')
async function checkLikeVtuber() {
  let params = new URLSearchParams(location.search)
  let id = params.get("id")
  try {
    let res = await fetch(`/user/${id}/vtuber`)
    json = await res.json()
    like = json.like.count
    userVtuber.querySelector("p").textContent = like
  } catch (error) {
    showFormResult({
      // form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
}
checkLikeVtuber()


let groupDisplay = document.querySelector('.group-detail-display')
let vtuberDisplay = groupDisplay.querySelector('.vtuber-display')
vtuberDisplay.remove()


async function loadGroups() {
  let params = new URLSearchParams(location.search)
  let id = params.get("id")
  let res = await fetch(`/user/${id}/favorite`)
  let json = await res.json()
  for (let vtuber of json.vtuber) {
    console.log(vtuber)
    showVtuber(vtuber)
  }
}

async function showVtuber(vtuber) {
  let VtuberItem = vtuberDisplay.cloneNode(true)
  VtuberItem.querySelector(".vtuber-link").href += "?id=" + vtuber.id
  VtuberItem.querySelector("img").src = vtuber.image
  VtuberItem.querySelector(".vtuber-name-span").textContent = vtuber.jpname
  groupDisplay.appendChild(VtuberItem)
}
loadGroups()



let allplaylist = document.querySelector(".allplaylist")
let swiperSlide = allplaylist.querySelector(".swiper-slide")
swiperSlide.remove()

async function loadVideo() {
  let params = new URLSearchParams(location.search)
  let id = params.get("id")
  let res = await fetch(`/video/${id}/favorite`)
  let json = await res.json()
  let videoRows = json.video
  for (let video of videoRows) {
    showVideo(video)
  }
}

async function showVideo(video) {
  let video_node = swiperSlide.cloneNode(true)
  if (video.image) {
    video_node.querySelector(".video-img").src = video.image
  }
  video_node.querySelector(".title").textContent = video.title
  video_node.querySelector(".videoUrl").href += "?id=" + video.id
  video_node.querySelector(".videoUrl2").href += "?id=" + video.id
  allplaylist.appendChild(video_node)
}

loadVideo()


$('.Video').click(function() {
  $('.Vtuber').removeClass("link-active")
  $('.Video').addClass("link-active")
  $('.group-detail-display').slideUp()
  setTimeout(() => {
    $('.clwp').slideDown()
  }, 400)
})


$('.Vtuber').click(function() {
  $('.Video').removeClass("link-active")
  $('.Vtuber').addClass("link-active")
  $('.clwp').slideUp()
  setTimeout(() => {
    $('.group-detail-display').slideDown()
  }, 400)
})