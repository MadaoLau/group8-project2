let arrow = document.querySelectorAll(".arrow");
for (var i = 0; i < arrow.length; i++) {
  arrow[i].addEventListener("click", (e) => {
    let arrowParent = e.target.parentElement.parentElement; //selecting main parent of arrow
    arrowParent.classList.toggle("showMenu");
  });
}

let sidebar = document.querySelector(".sidebar");
let sidebarBtn = document.querySelector(".bx-menu");
console.log(sidebarBtn);
sidebarBtn.addEventListener("click", () => {
  sidebar.classList.toggle("close");
});


function showFormResult({ form, title, message, isSuccess }) {
  let icon
  if (isSuccess) {
    form.reset()
    icon = 'success'
  } else {
    icon = 'error'
  }
  Swal.fire({
    title: title,
    text: message,
    icon: icon,
  })
}

async function loadUser() {
  let res = await fetch('/user')
  let json = await res.json()
  if (json.user) {
    document.querySelector('.admin_name').textContent = json.user.username
    document.querySelector('.profile_name').textContent = json.user.username
  }
  if (json.user.icon) {
    document.querySelector('.admin-icon').src = json.user.icon

    document.querySelector('.admin-icon2').src = json.user.icon
  }
}
loadUser()