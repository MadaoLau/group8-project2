let vtube_list = document.querySelector(".vtube_list")
let vtuber_template = vtube_list.querySelector(".vtuber")
vtuber_template.remove()

let addVTuberForm = document.querySelector("#add-vtuber-form")
let companyList2 = addVTuberForm.querySelector("#companyList")
let updataprofile = document.querySelector("#updataprofile")
let companyList = updataprofile.querySelector("#companyList")

let updatafrom = updataprofile.querySelector("#edit-vtuber-form")

function editVtuber(vtuber) {
  console.log("edit", vtuber)
  let fields = [
    "jp_vtuber_name",
    "cn_vtuber_name",
    "group_name",
    "image",
    "introduction",
    "bilibili",
    "period",
    "tags",
    "youtube",
    "twitter",
    "recommend",
    "url_3d",
  ]
  for (let field of fields) {
    updataprofile.querySelector("." + field).value = vtuber[field]
  }
  updataprofile.querySelector(".vtuber_id").value = vtuber.id
}

// let form = updataprofile.querySelector("form")
/*** 更新失敗 ***/
updatafrom.addEventListener("submit", async(event) => {
  event.preventDefault()
  let title = "update"
  let form = updatafrom
  let res = await fetch("/company")
  let companyRows = await res.json()
  let group_name = updatafrom.querySelector("#company").value
  let company = companyRows.find((row) => row.group_name == group_name)
  if (!company) {
    showFormResult({
      form,
      title,
      message: "VTuber Group Not Found",
      isSuccess: false,
    })
    return
  }
  form.vtuber_group_id.value = company.id
  body = new FormData(form)
  try {
    let res = await fetch("/vtuber/update", {
      method: form.method,
      body,
    })
    let json = await res.json()
    if (200 <= res.status && res.status < 300) {
      Swal.fire({
        title: title,
        text: json.message,
        icon: "success",
      }).then((result) => {
        loadVtuber()
      })
    } else {
      showFormResult({
        form,
        title,
        message: json.message,
        isSuccess: false,
      })
    }
  } catch (error) {
    showFormResult({
      form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
})



async function delVtuber(vtuber, vtuber_node) {
  console.log("del", vtuber.id)
    // TODO ask user to confirm
  let result = await Swal.fire({
      title: "Confirm to delete " + vtuber.jp_vtuber_name + " ?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
    },

    "",
    "question",
  )
  console.log(result)
  if (!result.isConfirmed) {
    return
  }
  let res = await fetch("/vtuber/" + vtuber.id, { method: "DELETE" })
  let json = await res.json()
  if (json.error) {
    // TODO show error
    Swal.fire("Failed to delete vtuber", json.error, "error")
    return
  }
  Swal.fire("Deleted " + vtuber.jp_vtuber_name, json.error, "error")
  vtuber_node.remove()
}

function showVtuber(vtuber) {
  let vtuber_node = vtuber_template.cloneNode(true)

  let fields = ["id", "jp_vtuber_name", "cn_vtuber_name", "group_name"]
  for (let field of fields) {
    vtuber_node.querySelector("." + field).textContent = vtuber[field]
  }
  vtuber_node.querySelector(".images").src = vtuber.image
  vtuber_node.querySelector(".edit_btn").addEventListener("click", () => {
    editVtuber(vtuber)
  })
  vtuber_node.querySelector(".del_btn").addEventListener("click", () => {
    delVtuber(vtuber, vtuber_node)
  })

  vtube_list.appendChild(vtuber_node)

  return
}

async function loadVtuber() {
  let res = await fetch("/vtuber")
  json = await res.json()
  console.log(json)
  vtube_list.querySelectorAll(".vtuber").forEach((e) => e.remove())
  for (let vtuber of json) {
    showVtuber(vtuber)
  }
}
loadVtuber()

async function searchVtuber(json) {
  // let res = await fetch("/admin/vtuber")
  // json = await res.json()
  console.log(json)
  vtube_list.querySelectorAll(".vtuber").forEach((e) => e.remove())
  for (let vtuber of json) {
    showVtuber(vtuber)
  }
}

async function initVTuber() {
  let res = await fetch("/company")
  let companyRows = await res.json()
  for (let company of companyRows) {
    let option = document.createElement("option")
    option.value = company.group_name
    companyList2.appendChild(option)
  }

  addVTuberForm.addEventListener("submit", async(event) => {
    event.preventDefault()
    let title = "createVtuber"
    let form = addVTuberForm

    let group_name = form.querySelector("#company").value
    let company = companyRows.find((row) => row.group_name == group_name)
    if (!company) {
      showFormResult({
        form,
        title,
        message: "VTuber Group Not Found",
        isSuccess: false,
      })
      return
    }
    form.vtuber_group_id.value = company.id
    let body = new FormData(form)
    try {
      let res = await fetch(form.action, {
          method: form.method,
          body,
        })
        // console.log('create memo res:', res)
      let json = await res.json()
        // console.log('create memo json:', json)
      if (200 <= res.status && res.status < 300) {
        showFormResult({ form, title, message: json.message, isSuccess: true })
        loadVtuber()
      } else {
        showFormResult({ form, title, message: json.message, isSuccess: false })
      }
    } catch (error) {
      showFormResult({
        form,
        title,
        message: error.toString(),
        isSuccess: false,
      })
    }
  })
}
initVTuber()

/*** 搜索失敗 及 ICON 沒反應***/
// document.querySelector('.bx-search').addEventListener('click', () => {
//   document.querySelector('.search').value
// })

let search = document.querySelector("form.search-box")
search.addEventListener("submit", async(event) => {
  event.preventDefault()
  let form = search
  let title = "search"
    // let body = {
    //   search: form.search.value,
    // }
  try {
    let params = new URLSearchParams()
    params.set("q", form.search.value)
    console.log(params.toString())
    let res = await fetch(`/admin/vtuber?${params}`)
    let json = await res.json()
    if (200 <= res.status && res.status < 300) {
      Swal.fire({
        title: title,
        text: json.message,
        icon: "success",
      }).then((result) => {
        searchVtuber(json)
      })
    } else {
      showFormResult({
        form,
        title,
        message: json.message,
        isSuccess: false,
      })
    }
  } catch (error) {
    showFormResult({
      form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
})