let user_list = document.querySelector(".user_list")
let user_template = user_list.querySelector(".user")
user_template.remove()

let addUserForm = document.querySelector(".register")
let updataprofile = document.querySelector("#updataprofile")
let updateProfileForm = updataprofile.querySelector("form")

updateProfileForm.addEventListener("submit", async (event) => {
  let form = updateProfileForm
  event.preventDefault()
  let title = "update"
  let admin = updataprofile.querySelector(".form-check-input:checked")
  if (!admin) {
    admin = false
  } else {
    admin = true
  }
  body = {
    user_id: form.user_id.value,
    username: form.username.value,
    password: form.password.value,
    email: form.email.value,
    is_admin: admin,
    exp : form.exp.value
  }
  // form.vtuber_group_id.value = company.id
  // body = new FormData(form)
  try {
    let res = await fetch("/user/update" , {
      method: form.method,
      headers: {
        "Content-Type": " application/json",
      },
      body: JSON.stringify(body),
    })
    let json = await res.json()
    if (200 <= res.status && res.status < 300) {
      Swal.fire({
        title: title,
        text: json.message,
        icon: "success",
      }).then((result) => {
        loadUserList()
      })
    } else {
      showFormResult({
        form,
        title,
        message: json.message,
        isSuccess: false,
      })
    }
  } catch (error) {
    showFormResult({
      form,
      title,
      message: error.toString(),
      isSuccess: false,
    })
  }
})

function editUser(user) {
  console.log("edit", user.id)
  user.user_id = user.id
  let fields = ["user_id", "username", "email", "exp"]
  let form = updateProfileForm
  for (let field of fields) {
    form[field].value = user[field]
  }
  form.password.value = ""
  if (user.is_admin == true) {
    updataprofile.querySelector(".form-check-input").checked = true
  } else {
    updataprofile.querySelector(".form-check-input").checked = false
  }
}

async function delUser(user, user_node) {
  console.log("del", user.id)
  // TODO ask user to confirm
  let result = await Swal.fire(
    {
      title: "Confirm to delete " + user.username + " ?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
    },

    "",
    "question",
  )
  console.log(result)
  if (!result.isConfirmed) {
    return
  }
  let res = await fetch("/user")
  let json = await res.json()
  if (json.user.id == user.id) {
    Swal.fire(
      "Failed to delete user",
      "you can not delete your account",
      "error",
    )
    return
  }
  if (user.id == 1) {
    Swal.fire(
      "Failed to delete user",
      "you can not delete the first account",
      "error",
    )
    return
  }
  res = await fetch("/user/" + user.id, { method: "DELETE" })
  json = await res.json()
  if (json.error) {
    // TODO show error
    Swal.fire("Failed to delete user", json.error, "error")
    return
  }
  Swal.fire("Deleted " + user.username, json.error, "error")
  user_node.remove()
}

function showUser(user) {
  let user_node = user_template.cloneNode(true)

  user_node.dataset.id = user.id

  let fields = ["id", "username", "email", "is_admin"]
  for (let field of fields) {
    user_node.querySelector("." + field).textContent = user[field]
  }
  if (user.icon) {
    user_node.querySelector(".images").src = user.icon
  }

  let form = updataprofile.querySelector("form")

  user_node.querySelector(".edit_btn").addEventListener("click", () => {
    editUser(user)
  })
  user_node.querySelector(".del_btn").addEventListener("click", () => {
    delUser(user, user_node)
  })
  user_list.appendChild(user_node)
  return
}

async function loadUserList() {
  let res = await fetch("/member")
  json = await res.json()
  console.log(json)
  if(json.error){
    Swal.fire('Cannot load user list',json.error,'error')
    return
  }
  user_list.querySelectorAll(".user").forEach((e) => e.remove())
  for (let user of json) {
    showUser(user)
  }
}
loadUserList()

async function initUser() {
  addUserForm.addEventListener("submit", async (event) => {
    event.preventDefault()
    let title = "register"
    let form = addUserForm
    let admin = addUserForm.querySelector(".form-check-input:checked")
    if (!admin) {
      admin = false
    } else {
      admin = true
    }
    let body = {
      email: form.email.value,
      username: form.username.value,
      password: form.password.value,
      confirmpassword: form.confirmpassword.value,
      is_admin: admin,
    }
    try {
      let res = await fetch("/register", {
        method: form.method,
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      })
      // console.log('create memo res:', res)
      let json = await res.json()
      // console.log('create memo json:', json)
      if (200 <= res.status && res.status < 300) {
        showFormResult({ form, title, message: json.message, isSuccess: true })
        loadUserList()
      } else {
        showFormResult({ form, title, message: json.message, isSuccess: false })
      }
    } catch (error) {
      showFormResult({
        form,
        title,
        message: error.toString(),
        isSuccess: false,
      })
    }
  })
}
initUser()
