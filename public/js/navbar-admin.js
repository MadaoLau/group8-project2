let navbar = document.querySelector('.sidebar.close')

navbar.innerHTML = /*html */ `<div class="logo-details">
<img src="images/logo.png" style="padding-left: 7px;height: 55px;" alt="">
</div>
<ul class="nav-links">
<li>
    <a href="admin.html" class="active">
        <i class='bx bx-grid-alt'></i>
        <span class="link_name">User</span>
    </a>
</li>
<li>
    <div class="iocn-link">
        <a href="#">
            <i class='bx bx-collection'></i>
            <span class="link_name">Video</span>
        </a>
    </div>
    <ul class="sub-menu">
        <li><a class="link_name" href="#">Video</a></li>
    </ul>
</li>
<li>
    <div class="iocn-link">
        <a href="#">
            <i class='bx bx-book-alt'></i>
            <span class="link_name">Images</span>
        </a>
    </div>
    <ul class="sub-menu">
        <li><a class="link_name" href="#">Images</a></li>
    </ul>
</li>
<li>
    <a href="#">
        <i class='bx bx-pie-chart-alt-2'></i>
        <span class="link_name">Music</span>
    </a>
    <ul class="sub-menu blank">
        <li><a class="link_name" href="#">Music</a></li>
    </ul>
</li>
<li>
    <div class="iocn-link">
    <a href="admin-vtuber.html">
        <i class='bx bx-user'></i>
        <span class="link_name">Vtuber</span>
        
    </a>
    <i class='bx bxs-chevron-down arrow'></i>
    </div>
    <ul class="sub-menu">
        <li><a href="admin-vtuber.html">Vtuber</a></li>
        <li><a href="admin-group.html">Vtuber Group</a></li>
    </ul>
</li>
<li>
    <a href="#">
        <i class='bx bx-message'></i>
        <span class="link_name">Messages</span>
    </a>
    <ul class="sub-menu blank">
        <li><a class="link_name" href="#">Messages</a></li>
    </ul>
</li>
<li>
    <a href="#">
        <i class='bx bx-heart'></i>
        <span class="link_name">Favrorites</span>
    </a>
    <ul class="sub-menu blank">
        <li><a class="link_name" href="#">Favrorites</a></li>
    </ul>
</li>
<li>
    <a href="#">
        <i class='bx bx-cog'></i>
        <span class="link_name">Setting</span>
    </a>
    <ul class="sub-menu blank">
        <li><a class="link_name" href="#">Setting</a></li>
    </ul>
</li>
<li>
    <div class="profile-details box-profile">
        <div class="profile-content">
            <img class="admin-icon" src="images/user.jpg" alt="profileImg">
        </div>
        <div class="name-job">
            <div class="profile_name">Amen</div>
            <div class="job">Admin</div>
        </div>
        <i class='bx bx-log-out' onclick="location.href = '/'"></i>
    </div>
</li>
</ul>`