let group_list = document.querySelector(".group_list")
let group_template = group_list.querySelector(".group")
group_template.remove()

async function loadGroup() {
  let res = await fetch("/company")
  json = await res.json()
  console.log(json)
  group_list.querySelectorAll(".group").forEach((e) => e.remove())
  for (let group of json) {
    showGroup(group)
  }
}
loadGroup()



function showGroup(group) {
  let group_node = group_template.cloneNode(true)

  let fields = ["id", "group_name"]
  for (let field of fields) {
    group_node.querySelector("." + field).textContent = group[field]
  }
  if (group.group_image) {
    group_node.querySelector('.images').src = group.group_image
  }
  group_node.querySelector(".edit_btn").addEventListener("click", () => {
    editGroup(group)
  })
  group_node.querySelector(".del_btn").addEventListener("click", () => {
    delGroup(group, group_node)
  })

  group_list.appendChild(group_node)

  return
}


let addGroupForm = document.querySelector("#add-group-form")

async function initGroup() {
  addGroupForm.addEventListener("submit", async(event) => {
    event.preventDefault()
    let title = "create Group"
    let form = addGroupForm
    let body = new FormData(form)
    try {
      let res = await fetch(form.action, {
          method: form.method,
          body,
        })
        // console.log('create memo res:', res)
      let json = await res.json()
        // console.log('create memo json:', json)
      if (200 <= res.status && res.status < 300) {
        showFormResult({ form, title, message: json.message, isSuccess: true })
        loadGroup()
      } else {
        showFormResult({ form, title, message: json.message, isSuccess: false })
      }
    } catch (error) {
      showFormResult({
        form,
        title,
        message: error.toString(),
        isSuccess: false,
      })
    }
  })
}
initGroup()



let updateform = document.querySelector('#edit-group-form')


updateform.addEventListener('submit', async event => {
  event.preventDefault()
  let title = 'update'
  let form = updateform
  let body = new FormData(form)
  try {
    let res = await fetch("/group/update", {
      method: form.method,
      body,
    })
    let json = await res.json()
    if (200 <= res.status && res.status < 300) {
      Swal.fire({
        title: title,
        text: json.message,
        icon: 'success',
      }).then((result) => {
        loadGroup()
      })
    } else {
      showFormResult({
        form,
        title,
        message: json.message,
        isSuccess: false
      })
    }
  } catch (error) {
    showFormResult({
      form,
      title,
      message: error.toString(),
      isSuccess: false
    })
  }
})

function editGroup(group) {
  console.log("edit", group.id)
  let fields = ["group_name", "group_image"]
  for (let field of fields) {
    updateform.querySelector("." + field).value = group[field]
  }
  updateform.querySelector(".group_id").value = group.id

}



async function delGroup(group, group_node) {
  console.log("del", group.id)
  let result = await Swal.fire({
      title: "Confirm to delete " + group.group_name + " ?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
    },

    "",
    "question",
  )
  console.log(result)
  if (!result.isConfirmed) {
    return
  }
  let res = await fetch("/group/" + group.id, { method: "DELETE" })
  let json = await res.json()
  if (json.error) {
    // TODO show error
    Swal.fire("Failed to delete group", json.error, "error")
    return
  }
  Swal.fire("Deleted " + group.group_name, json.error, "error")
  group_node.remove()
}