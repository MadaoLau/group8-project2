let searchParams = new URLSearchParams(location.search)
let id = searchParams.get("singerId")

function showFormResult({ form, title, message, isSuccess }) {
    let icon;
    if (isSuccess) {
      if(form){
        form.reset();
      }
      icon = "success";
    } else {
      icon = "error";
    }
    Swal.fire({
      title: title,
      text: message,
      icon: icon,
    });
  }

let likeButton = document.querySelectorAll(".like")
for (let i=0; i<likeButton.length; i++){
  likeButton[i].addEventListener("click", ()=>{
    likeButton[i].classList.toggle("bg-red");
  })
}

async function loadViewCount(){
    let res = await fetch(`/singer/${id}/session`)
    let json = await res.json()
    console.log(json.views)
  }

  let singerName = document.querySelector(".singer-name")
  function createSingerName(singer){
    singerName.textContent = `${singer.singer_name}`
  }

  let singerSharePart = document.querySelector(".singer-share-part")
  function createSingerSharePart(singer) {
    let div = document.createElement("div")
    div.className = "display-flex singer-details"
    div.innerHTML = /* html */ `
    <div class="top-line">
    <div class="singer-music-name">${singer.music_name}</div>
  </div>
  <div class="outside-singer-photo">
      ${
        singer.music_photo
          ? `<img src="${singer.music_photo}" class="singer-share-photo">`
          : ""
      }
  </div>
  <audio controls src="${
      singer.music_file
    }" type="audio/mpeg" class="singer-share-file"></audio>
    `
    singerSharePart.appendChild(div)
  }
  
  let reply = document.querySelector("#comment")
  reply.addEventListener("click", () => {
    document.querySelector(".commentInfo").style.display = "block"
  })
  
  let singerPostSubmit = document.querySelector("form#singer-comment-text")
  singerPostSubmit.addEventListener("submit", async (event) => {
  event.preventDefault()
  let form = event.target
  let formObject = {}
  formObject["singercomment"] = form.singercomment.value
  formObject["singerId"] = id

  try {
    let res = await fetch(`/singer/${id}/comment`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formObject),
    })
    let json = await res.json()
    if (200 <= res.status && res.status < 300) {
      showFormResult({ form, message: json.message, isSuccess: true })
      loadSingerComment();
    } else {
      showFormResult({ form, message: json.message, isSuccess: false })
    }

  } catch (error) {
    showFormResult({ form, message: "Please type comment", isSuccess: false })
  }
})

let commentList = document.querySelector(".comment-list")
function postSingerComment(singerPost) {
  let date = new Date(singerPost.updated_at)
  let dateOfYear = date.getFullYear()
  let dateOfMonth = date.getMonth() + 1
  let dateOfDay = date.getDate()
  let dateOfHour = date.getHours()
  let dateOfMins = date.getMinutes()
  let li = document.createElement("li")
  li.className = "comments"
  li.innerHTML = /* html */ `
    <div class="one-comment display-flex">
    <div class="user-details">
        <div class="user-icon">
        ${singerPost.icon ? `<img src="${singerPost.icon}">` : "user-icon"}
        </div>
        <div class="username">${singerPost.username}</div>
    </div>
    <div class="comment-details">
        <div class="comment-text singer-share-reply">
            <p>${singerPost.content}</p>
        </div>
        <div class="updated-time display-flex">
          <div class="year">${dateOfYear}-</div>
          <div class="month">${dateOfMonth}-</div>
          <div class="day">${dateOfDay}</div>
          <div class="space"></div>
          <div class="hour">${dateOfHour}:</div>
          <div class="mins">${dateOfMins}</div>
        </div>

        <div class="media-area">
            <button class="like">like</button>
        </div>
    </div>
</div>
    `
  commentList.appendChild(li) 
}

async function loadSingerComment() {
    let res = await fetch(`/singer/${id}/comment`)
    let json = await res.json()
    commentList.innerHTML = ""
    for (let singerPost of json.singerPostList) {
      if(singerPost.singer_id == id){
        postSingerComment(singerPost)
      }
    }
  }
  
  async function loadSingerSharePage() {
    let res = await fetch("/music/singer/create")
    let json = await res.json()
    // console.log(json.createSingerList)
    singerSharePart.innerHTML = ""
    for (let singer of json.createSingerList) {
      if (singer.id == id) {
        createSingerSharePart(singer)
        createSingerName(singer)
      }
    }
  }

  async function loadSingerViewCount(){
    let res = await fetch(`/singer/${id}/session`)
    let json = await res.json()
    console.log(json.views)
  }

  window.onload = async function () {
    await loadSingerSharePage()
    await loadSingerComment()
    await loadSingerViewCount()
  }
